from __future__ import annotations
from typing import Dict, Union, Optional, Type

import os
from pathlib import Path
from os import PathLike
from enum import IntEnum
from collections import UserString

import toml
import cachetools
import babel
from iso639 import languages


INSTALLED_LOCALES: Dict[str, Locale] = {}


@cachetools.cached(cache=cachetools.LRUCache(maxsize=100))
def list_locale_names(locale: str) -> Dict[str, str]:
    """Returns a dict of all allowed locales and their localized name.

    Args:
        locale: The locale to localize the locale names to.
    """

    return {
        lang.alpha2: babel.core.Locale(lang.alpha2).get_display_name(locale)
        for lang in languages
        if len(lang.alpha2) > 0
    }


class Message:
    def __init__(self, other: str, one: Optional[str] = None, **kwargs):
        self.other = other
        self.one = one


class CatalogMergeMode(IntEnum):
    keep_old = 1
    use_new = 2


class Catalog:
    def __init__(self, path: PathLike, language: str):
        self.messages = {}

    @classmethod
    def load(cls: Type[Catalog], path: Path) -> Catalog:
        with path.open("r") as fp:
            catalog = Catalog(path, language=path.stem)

            raw_messages = toml.load(fp)

            for k, v in raw_messages.items():
                catalog.add_message(k, Message(**v))

            return catalog

    def add_message(self, id: str, message: Message):
        self.messages.update({id: message})

    def merge(
        self,
        other: Catalog,
        merge_mode: CatalogMergeMode = CatalogMergeMode.use_new,
    ):
        """Merges the entries from the other catalog into this catalog.

        Args:
            other: The catalog to merge into the values from.
            merge_mode: Defines whether to keep the current values or to
                override with the values from other.
        """
        if merge_mode == CatalogMergeMode.use_new:
            self.messages.update(other.messages)
        else:
            for key, val in other.messages.items():
                if key not in self.messages:
                    self.messages[key] = val

    def gettext(self, message: str) -> str:
        try:
            return self.messages[message].other
        except KeyError:
            if message is None:
                return message

            return message.replace("\\n", "\n")

    def ngettext(self, message: str, num: int) -> str:
        msg: Message = self.messages[message]

        return msg.one if num == 1 and msg.one else msg.other


class Locale:
    def __init__(
        self,
        base_path: os.PathLike,
        language: str,
    ):
        self.catalogs = {
            "messages": Catalog.load(Path(base_path) / f"{language}.toml")
        }
        self.language = language

    def gettext(self, message: str, domain: Optional[str] = "messages") -> str:
        """Translate the given message.

        Args:
            msg: The message to translate.
            domain: The domain to get the translations from.
        """
        return self.catalogs[domain].gettext(message)

    def ngettext(
        self,
        message: str,
        message_plural: str,
        num: int,
        domain: Optional[str] = "messages",
    ) -> str:
        """Translate the given message.
        Automatically use either the singular or plural message.

        Args:
            singular: The message to translate if ``num``==1.
            plural: The message to translate if ``num``!=1.
            num: Decides whether plural or singular is used for translation.
            domain: The domain to get the translations from.
        """
        return self.catalogs[domain].ngettext(message, message_plural, num)


def install_locale(locale: str):
    print(
        f"-- Installing locale: {locale}",
        Path(__file__).parent / "translations" / f"{locale}.toml",
    )
    INSTALLED_LOCALES.update(
        {locale: Locale(Path(__file__).parent / "translations", locale)}
    )


def gettext(
    message: str, locale: str, domain: Optional[str] = "messages"
) -> str:
    """Translate the given string using the locale for the current request.

    Args:
        message: The message to translate.
    """

    try:
        return INSTALLED_LOCALES[locale].gettext(message, domain=domain)
    except KeyError:
        if message is None:
            return message

        return message.replace("\\n", "\n")


def ngettext(
    singular: str,
    plural: str,
    num: Union[int, float],
    locale: str,
    domain: Optional[str] = "messages",
) -> str:
    """Translate the given string using the locale for the current request.

    Args:
        singular: The string to use when ``num``==1.
        plural: The fallback string when ``num``!=1.
        num: Decides whether plural or singular is used for translation.
    """

    try:
        return INSTALLED_LOCALES[locale].ngettext(
            singular, plural, num, domain=domain
        )
    except KeyError:
        return plural if num != 1 else singular


_ = gettext


class LazyBase(UserString):
    """Base object for delayed translations. Fully compatible to ``str``.

    ``UserString`` tries to access self.data in each of its functions, so just
    making data a property that executes ``__str__()`` is the easiest way to
    make lazy evaluation possible.

    Note: __str__() must be overloaded for each class inheriting from this.
    """

    @property
    def data(self):
        # UserString tries to access self.data in each of its functions, so
        # just making data a property that executes __str__() is the easiest
        # way to make lazy evaluation possible.
        #
        # __str__() must be overloaded for each lazy evaluation class.

        return self.__str__()

    @classmethod
    def __get_validators__(cls):
        yield

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(pattern="*")


class lazy_gettext(LazyBase):
    """A gettext message with delayed translation. Useful when declaring
    messages outside of a request context.
    """

    def __init__(self, message: str):
        self.message: str = message

    def __str__(self) -> str:
        return gettext(self.message, locale="en")

    def localize(self, locale: str, domain: Optional[str] = "messages"):
        return gettext(self.message, locale, domain)


class lazy_ngettext(LazyBase):
    """A ngettext message with delayed translation. Useful when declaring
    messages outside of a request context.
    """

    def __init__(self, singular: str, plural: str, num: Union[int, float]):
        self.singular: str = singular
        self.plural: str = plural
        self.num: Union[int, float] = num

    def __str__(self) -> str:
        return ngettext(self.singular, self.plural, self.num)

    def localize(self, locale: str, domain: Optional[str] = "messages"):
        return ngettext(self.singular, self.plural, self.num, locale, domain)


N_ = lazy_gettext
