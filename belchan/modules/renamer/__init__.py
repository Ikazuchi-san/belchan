from belchan.context import ContextBase
from belchan.models.docs import CommandDocumentation
from typing import Optional, List

import re
import random

import discord
import cachetools
from random_username.generate import generate_username

from belchan.application import Application
from belchan.models.base import Field, validator
from belchan.models.enum import Enum
from belchan.models.config import ModuleConfig, ServerConfig
from belchan.models.discord import Event
from belchan.modules import Module
from belchan import commands as dcommands
from belchan.i18n import _, N_

pattern_cache: cachetools.TTLCache = cachetools.TTLCache(maxsize=100, ttl=3600)


@cachetools.cached(cache=pattern_cache)
def get_compiled_pattern(pattern: str) -> re.Pattern:
    return re.Compile(pattern)


class BadNameMatchMode(str, Enum):
    pattern: str = "pattern"
    blacklist: str = "blacklist"
    all: str = "all"


class BadNameReplaceMode(str, Enum):
    list_random: str = "list"
    generated: str = "generated"


def validate_pattern(pattern: str):
    re.compile(pattern, re.IGNORECASE | re.UNICODE)

    return pattern


class RenamerConfig(ModuleConfig):
    enabled: Optional[bool] = Field(
        False, description="Defines whether a module is enabled."
    )
    detection_mode: BadNameMatchMode = Field(
        BadNameMatchMode.pattern,
        description="Which method to use for bad name detection.",
    )
    bad_name_pattern: Optional[str] = Field(
        None, description="The pattern to use for detecting a bad name."
    )
    bad_names_list: Optional[List[str]] = Field(
        [], description="A list of disallowed (bad) names."
    )
    replacement_mode: BadNameReplaceMode = Field(
        BadNameReplaceMode.generated,
        description="Which method to use for renaming users.",
    )
    replacements_list: Optional[List[str]] = Field(
        ["Please change your name"],
        description="A list of possible replacement names.",
    )
    dm_title: Optional[str] = Field(
        "Your nickname has been changed.",
        description=(
            "The title line of the message that will be sent to renamed"
            "users."
        ),
    )
    dm_content: Optional[str] = Field(
        "Your nickname has been changed to {new_name} due to not complying with"
        " {server.name}'s naming rules.",
        description=(
            "The content of the message that will be sent to renamed users."
        ),
    )

    validator("bad_name_pattern", allow_reuse=True)(validate_pattern)

    class Config:
        validate_assignment = True


class Renamer(
    Module, doc=N_("Name restrictions and renaming."), config=RenamerConfig
):
    ###########################################################################
    # Event handlers

    @dcommands.listener(Event.member_join)
    async def handle_join(self, member: discord.Member):
        await self.__handle_rename(member)

    @dcommands.listener(Event.member_update)
    async def handle_update(
        self, before: discord.Member, after: discord.Member
    ):
        await self.__handle_rename(after)

    async def __handle_rename(self, member: discord.Member):
        cfg: RenamerConfig = self.bot.get_module_config(
            member.guild.id, "renamer"
        )
        scfg: ServerConfig = self.bot.get_server_config(member.guild.id)

        # Don't run if module is not enabled.
        if not cfg.enabled:
            return

        bad_name: bool = False

        if (
            cfg.match_mode == BadNameMatchMode.pattern
            or cfg.match_mode == BadNameMatchMode.all
        ) and cfg.bad_name_pattern is not None:
            matcher = get_compiled_pattern(cfg)

            bad_name = matcher.match(member.display_name) is None
        elif (
            cfg.match_mode == BadNameMatchMode.blacklist
            or cfg.match_mode == BadNameMatchMode.all
        ):
            if bad_name:
                pass
            else:
                bad_name = member.display_name in cfg.bad_names_list

        if bad_name:
            replacement_name = None
            old_name = member.display_name

            if cfg.replacement_mode == BadNameReplaceMode.list_random:
                replacement_name = random.choice(cfg.replacements_list)
            else:
                replacement_name = generate_username(1)[0]

            try:
                await member.edit(
                    nick=replacement_name,
                    reason=_("Bad name match.", locale=scfg.locale),
                )

                self.bot.logger.info(
                    f"[{self.qualified_name}] [Server: {member.guild.name}"
                    f" ({member.guild.id})] Renaming {member.display_name} =>"
                    f" {replacement_name}"
                )
            except discord.errors.Forbidden:
                self.bot.logger.warning(
                    f"[{self.qualified_name}] [Server: {member.guild.name}"
                    f" ({member.guild.id})] Renaming {member.display_name} =>"
                    f" {replacement_name} failed. (Missing permissions)"
                )

                return

            try:
                embed: discord.Embed = discord.Embed(
                    title=cfg.dm_title.format(
                        user=member,
                        server=member.guild,
                        old_name=old_name,
                        new_name=replacement_name,
                    ),
                    description=cfg.dm_content.format(
                        user=member,
                        server=member.guild,
                        old_name=old_name,
                        new_name=replacement_name,
                    ),
                )

                await member.send(embed=embed)
            except discord.errors.Forbidden:
                self.bot.logger.info(
                    f"[{self.qualified_name}] [Server: {member.guild.name}"
                    f" ({member.guild.id})] Renaming {member.display_name} =>"
                    f" {replacement_name}. [DM FAILED]"
                )

    @dcommands.command(
        doc=CommandDocumentation(command="rename", doc=N_("Rename a user."))
    )
    async def rename(self, ctx: ContextBase):
        replacement_name = generate_username(1)[0]

        try:
            await ctx.author.edit(
                nick=replacement_name,
                reason=_("Reame command.", locale=ctx.locale),
            )
        except discord.errors.Forbidden:
            await ctx.send(
                _(
                    "Command failed due to missing permissions.",
                    locale=ctx.locale,
                )
            )


def setup(bot: Application):
    bot.add_module(Renamer(bot))


def teardown(bot: Application):
    bot.remove_module(Renamer.qualified_name)
