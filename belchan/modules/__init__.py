"""Module related functions and classes.
"""

from __future__ import annotations
from typing import List, TYPE_CHECKING, Tuple, Sequence

import inspect
from pathlib import Path
from abc import ABCMeta

import textwrap
from tabulate import tabulate

from ..commands import commands
from ..context import ContextBase
from ..models.config import ModuleConfig
from ..i18n import lazy_gettext, _

if TYPE_CHECKING:
    from ..application import Application


class MissingDocumentationError(Exception):
    def __init__(self, module: str):
        self.module = module

    def __str__(self) -> str:
        return f"Module {self.module} is missing the 'doc' attribute."


class ModuleMeta(commands.CogMeta, ABCMeta):
    """Metaclass for modules, sets specific attributes."""

    def __new__(cls, name, bases, namespace, **kwargs):
        new_cls = super().__new__(cls, name, bases, namespace)

        if "doc" in kwargs:
            doc = kwargs["doc"]
        else:
            doc = kwargs.get("description", None)

        if not isinstance(doc, lazy_gettext):
            doc = lazy_gettext(doc)

        setattr(new_cls, "__module_doc__", doc)
        setattr(new_cls, "__permissions__", kwargs.get("permissions", []))
        setattr(
            new_cls, "__config_template__", kwargs.get("config", ModuleConfig)
        )
        setattr(
            new_cls, "__database_tables__", kwargs.get("database_tables", [])
        )
        setattr(new_cls, "__bundles__", kwargs.get("bundles", []))

        return new_cls


class Module(commands.Cog, metaclass=ModuleMeta):
    """A loadable set of commands, listeners and tasks.

    Requires a `doc` meta-attribute to be provided.

    See: https://discordpy.readthedocs.io/en/latest/ext/commands/api.html#cog

    Attributes:
        qualified_name: The qualified name of the module.

    Example:
        A simple module that provides a `ping` command::

            from belchan.modules import Module, ContextBase
            from belchan.i18n import N_
            from belchan.commands import command
            from belchan.models.docs import CommandDocumentation

            class MyModule(Module, doc=_("My command list")):
                @command(doc=CommandDocumentation(command="ping",
                    doc=N_("Replies with `pong`.")))
                async def ping(self, ctx: ContextBase):
                    await ctx.send("pong")
    """

    def __init__(self, bot: Application):
        self.bot = bot

        if self.__module_doc__ is None:
            raise MissingDocumentationError(self.qualified_name)

        if len(self.__database_tables__) > 0:
            bot.database.ensure_tables(self.__database_tables__)

        for bundle in self.__bundles__:
            bundle.path = Path(self.bot.root_bundle.path, bundle.path)
            bundle.ensure()

    def list_locales(self) -> Sequence[Path]:
        """List all translations available for the module."""
        return [
            l
            for l in (
                Path(inspect.getfile(self.__class__)).parent / "translations"
            ).glob("*.toml")
        ]

    @classmethod
    def get_description(cls):
        """Return the description (documentation) of the module."""
        return cls.__module_doc__

    def cog_unload(self):
        """Alias function that is called whenever the module is unloaded.

        Use :method:`on_module_unload` instead.
        """
        self.on_module_unload()

    def on_module_unload(self):
        """Hook function that is called when the module is unloaded.

        Can be used to execute code when the module is unloaded or reloaded, or
        the bot is shut down.

        One example of this is to stop all running tasks of a module on unload.
        """
        pass

    async def make_help(
        self, ctx: ContextBase
    ) -> Tuple[commands.Paginator, commands.Paginator]:
        """Returns the help for the module.

        - Module documentation is shown
        - Commands only shown as <command name> <short help>
        """
        hdr: commands.Paginator = commands.Paginator(prefix="", suffix="")

        hdr.add_line(f"**{self.qualified_name}**:")
        hdr.add_line(self.__module_doc__.localize(ctx.locale), empty=True)
        hdr.add_line(_("**Commands**:", locale=ctx.locale))

        table_rows: List[Tuple[str, str]] = []

        cmds: commands.Paginator = commands.Paginator(prefix="```apache")

        for command in self.get_commands():
            # Do not show hidden commands.
            if command.hidden:
                continue

            # Only add commands the user is actually allowed to run in the
            # context to the list.
            try:
                if await command.can_run(ctx):
                    table_rows.append(await command.make_short_help_row(ctx))
            except commands.CheckFailure:
                pass

        if len(table_rows) > 0:
            for line in tabulate(
                table_rows, tablefmt="plain", maxcolwidths=[None, 60]
            ).splitlines():
                cmds.add_line(line)

        return (hdr, cmds)

    async def make_short_help(self, ctx: ContextBase) -> str:
        """Returns the short help for the module.

        - Module documentation isn't shown
        - Commands only shown as <command name> <short help>
        """
        if len(self.get_commands()) == 0:
            return ""

        ret: str = f"{self.qualified_name}:\n"

        table_rows: List[Tuple[str, str]] = []

        for command in self.get_commands():
            # Do not show hidden commands.
            if command.hidden:
                continue

            # Only add commands the user is actually allowed to run in the
            # context to the list.
            try:
                if await command.can_run(ctx):
                    table_rows.append(await command.make_short_help_row(ctx))
                else:
                    pass
            except commands.CheckFailure:
                pass

        if len(table_rows) == 0:
            return ""

        ret += textwrap.indent(tabulate(table_rows, tablefmt="plain"), "    ")

        return ret
