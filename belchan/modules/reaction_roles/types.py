from typing import Union

import discord

AnyEmoji = Union[discord.Emoji, discord.PartialEmoji, str]
