from typing import (
    Any,
    List,
    Union,
    Tuple,
    TypeVar,
    Optional,
    get_args,
    get_origin,
)

import os
import importlib
import inspect
import json
from datetime import datetime


import discord
from discord.ext.commands.converter import Greedy
from discord.ext.commands.help import Paginator
from pydantic.error_wrappers import ValidationError
from tabulate import tabulate

from belchan import commands as dcommands
from belchan.database import db_session
from belchan.application import Application
from belchan.context import ContextBase
from belchan.modules import Module
from belchan.models.database import CommandLog
from belchan.models.permissions import ModulePermission
from belchan.models.config import ModuleConfig, ServerConfig, ConfigBase
from belchan.models.docs import (
    CommandDocumentation,
    ArgumentDocumentation,
    GroupDocumentation,
)
from belchan.models.enum import Enum
from belchan.i18n import _, lazy_gettext as N_
from belchan.converters import DateTime, Duration

from . import config

T = TypeVar("T")

__all__ = ["Config", "setup", "teardown"]


class ModeEnum(str, Enum):
    """Mode accecpted values for the config command."""

    get: str = "get"
    set: str = "set"
    add: str = "add"
    remove: str = "remove"
    unset: str = "unset"


class Config(Module, doc=N_("Module- and server-configuration commands.")):
    def __init__(self, bot: Application):
        super().__init__(bot)

    @dcommands.group(
        doc=GroupDocumentation(
            command="config",
            doc=N_("Set, update and read configuration values."),
            args=[
                ArgumentDocumentation(
                    name="module", doc=N_("The module to configure.")
                ),
                ArgumentDocumentation(
                    name="option",
                    doc=N_("The configuration option to view or modify."),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="value",
                    doc=N_(
                        "The new value to set the option to or add/remove"
                        " from the option."
                    ),
                    optional=True,
                ),
            ],
        ),
        invoke_without_command=True,
    )
    @dcommands.has_guild_permissions(administrator=True)
    async def config(self, ctx: ContextBase, *, rest: Optional[str] = None):
        if ctx.invoked_subcommand is None and rest is None:
            await self.module_list(ctx)
        else:
            await self.config_get(ctx, *rest.split())

    @config.command(
        doc=CommandDocumentation(
            command="get",
            doc=N_("View configuration values."),
            args=[
                ArgumentDocumentation(
                    name="module",
                    doc=N_("The module to view the configuration of."),
                ),
                ArgumentDocumentation(
                    name="option",
                    doc=N_("The configuration option to view."),
                    optional=True,
                ),
            ],
        ),
        name="get",
    )
    async def config_get(
        self, ctx: ContextBase, module: str, option: Optional[str] = None
    ):
        await self.__config(ctx, module, option, ModeEnum.get)

    @config.command(
        doc=CommandDocumentation(
            command="set",
            doc=N_("Set configuration options."),
            args=[
                ArgumentDocumentation(
                    name="module", doc=N_("The module to configure.")
                ),
                ArgumentDocumentation(
                    name="option",
                    doc=N_("The configuration option to modify."),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="value",
                    doc=N_("The new value to set the option to."),
                    optional=True,
                ),
            ],
        ),
        name="set",
    )
    async def config_set(
        self,
        ctx: ContextBase,
        module: str,
        option: str,
        *,
        value: str,
    ):
        await self.__config(ctx, module, option, ModeEnum.set, value=value)

    @config.command(
        doc=CommandDocumentation(
            command="unset",
            doc=N_("Unset configuration options."),
            args=[
                ArgumentDocumentation(
                    name="module", doc=N_("The module to configure.")
                ),
                ArgumentDocumentation(
                    name="option",
                    doc=N_("The configuration option to modify."),
                    optional=True,
                ),
            ],
        ),
        name="unset",
    )
    async def config_unset(self, ctx: ContextBase, module: str, option: str):
        await self.__config(ctx, module, option, ModeEnum.unset)

    @config.command(
        doc=CommandDocumentation(
            command="add",
            doc=N_("Add values to configuration options."),
            args=[
                ArgumentDocumentation(
                    name="module", doc=N_("The module to configure.")
                ),
                ArgumentDocumentation(
                    name="option",
                    doc=N_("The configuration option to modify."),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="value",
                    doc=N_("The value to add to the option."),
                    optional=True,
                ),
            ],
        ),
        name="add",
    )
    async def config_add(
        self,
        ctx: ContextBase,
        module: str,
        option: str,
        *,
        value: str,
    ):
        await self.__config(ctx, module, option, ModeEnum.add, value=value)

    @config.command(
        doc=CommandDocumentation(
            command="remove",
            doc=N_("Remove values from configuration options."),
            args=[
                ArgumentDocumentation(
                    name="module", doc=N_("The module to configure.")
                ),
                ArgumentDocumentation(
                    name="option",
                    doc=N_("The configuration option to modify."),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="value",
                    doc=N_("The value to remove from the option."),
                    optional=True,
                ),
            ],
        ),
        name="remove",
    )
    async def config_remove(
        self,
        ctx: ContextBase,
        module: str,
        option: str,
        *,
        value: str = None,
    ):
        await self.__config(ctx, module, option, ModeEnum.remove, value=value)

    @config.command(
        doc=CommandDocumentation(
            command="help",
            doc=N_("Show help for configuration options."),
            args=[
                ArgumentDocumentation(
                    name="module",
                    doc=N_("The module to view the configuration of."),
                ),
                ArgumentDocumentation(
                    name="option",
                    doc=N_("The configuration option to view."),
                    optional=True,
                ),
            ],
        ),
        name="help",
    )
    async def config_help(
        self, ctx: ContextBase, module: str, option: Optional[str] = None
    ):
        if module == "server":
            cfg: ServerConfig = self.bot.get_server_config(ctx.guild.id)
        else:
            cfg: ModuleConfig = self.bot.get_module_config(ctx.guild.id, module)

        if option:
            try:
                description: str = ""

                info = cfg.__fields__[option].field_info
                type_name: str = config.get_type_name(info)

                if info.description:
                    description = "\n".join(
                        [l.lstrip() for l in info.description.splitlines()]
                    )

                await ctx.send(
                    f"**{option}** `{type_name}`:```\n" f"{description}\n```"
                )
            except KeyError:
                await ctx.send(
                    _(
                        "Invalid `option`. See below for a complete list of"
                        " available options.",
                        locale=ctx.locale,
                    )
                )

                return await self.config_get(ctx, module)
        else:
            table_rows: List[Tuple[str, str, str]] = []

            for field, info in cfg.__fields__.items():
                description: str = ""

                if info.field_info.description:
                    description = "\n".join(
                        [
                            l.lstrip()
                            for l in info.field_info.description.splitlines()
                        ]
                    )

                table_rows.append(
                    (
                        field,
                        config.get_type_name(info),
                        description,
                    )
                )

            pg = Paginator()

            for line in tabulate(
                table_rows,
                headers=(
                    _("Option", locale=ctx.locale),
                    _("Type", locale=ctx.locale),
                    _("Description", locale=ctx.locale),
                ),
                maxcolwidths=[None, None, 60],
            ).splitlines():
                pg.add_line(line)

            for page in pg.pages:
                await ctx.send(page)

    async def __config(
        self,
        ctx: ContextBase,
        module: str,
        option: Optional[str] = None,
        mode: ModeEnum = ModeEnum.get,
        *,
        value: Optional[Any] = None,
    ):
        # Turns out this is a really bad way to do it due to early casting.
        #
        # TODO : Rewrite to use pydantic-based type conversion instead.

        try:
            if module == "server":
                await Config.__configure_server(
                    ctx, option=option, mode=mode, value=value
                )
            else:
                await Config.__configure_module(
                    ctx, module=module, option=option, mode=mode, value=value
                )
        except ValidationError:
            await ctx.channel.send(_("Invalid value.", locale="locale"))

    @dcommands.group(
        doc=GroupDocumentation(command="module", doc=N_("Module management."))
    )
    @dcommands.is_owner()
    async def module(self, ctx: ContextBase):
        if ctx.invoked_subcommand is None:
            for page in (await self.module.make_help(ctx)).pages:
                await ctx.send(page)

    @module.command(
        doc=CommandDocumentation(
            command="load",
            doc=N_("Load a bot module."),
            args=[
                ArgumentDocumentation(
                    name="module",
                    doc=N_(
                        "The module to load. Must be a valid folder name"
                        " in the modules folder."
                    ),
                )
            ],
        ),
        name="load",
    )
    @dcommands.is_owner()
    async def module_load(self, ctx: ContextBase, module: str):
        try:
            self.bot.load_module(module.lower())

            await ctx.channel.send(
                _("Module `{}` has been loaded!", locale=ctx.locale).format(
                    module
                )
            )
        except dcommands.errors.ExtensionAlreadyLoaded:
            await ctx.channel.send(
                _("Module `{}` is already loaded!", locale=ctx.locale).format(
                    module
                )
            )
        except dcommands.errors.ExtensionNotFound:
            await ctx.channel.send(
                _("Module `{}` could not be found!", locale=ctx.locale).format(
                    module
                )
            )
        except dcommands.errors.ExtensionFailed as err:
            await ctx.channel.send(
                _(
                    "An error occured while trying to load module `{}`:",
                    ctx.locale,
                ).format(module)
            )

            pg: dcommands.Paginator = dcommands.Paginator(prefix="```python")

            for line in str(err).splitlines():
                pg.add_line(line)

            for page in pg.pages:
                await ctx.channel.send(page)

    @module.command(
        doc=CommandDocumentation(
            command="unload",
            doc=N_("Unload a bot module."),
            args=[
                ArgumentDocumentation(
                    name="module",
                    doc=N_(
                        "The module to unload. Must be a valid folder"
                        " name in the modules folder"
                    ),
                )
            ],
        ),
        name="unload",
    )
    @dcommands.is_owner()
    async def module_unload(self, ctx: ContextBase, module: str):
        try:
            self.bot.unload_module(module.lower())

            await ctx.channel.send(
                _("Module `{}` has been unloaded!", locale=ctx.locale).format(
                    module
                )
            )
        except dcommands.errors.ExtensionNotLoaded:
            await ctx.channel.send(
                _(
                    "Cannot unload module `{}`: Not loaded!",
                    locale=ctx.locale,
                ).format(module)
            )
        except dcommands.errors.ExtensionNotFound:
            await ctx.channel.send(
                _("Module `{}` could not be found!", locale=ctx.locale).format(
                    module
                )
            )
        except dcommands.errors.ExtensionFailed as err:
            await ctx.channel.send(
                _(
                    "An error occured while trying to load module `{}`:",
                    ctx.locale,
                ).format(module)
            )

            pg: dcommands.Paginator = dcommands.Paginator(prefix="```python")

            for line in str(err).splitlines():
                pg.add_line(line)

            for page in pg.pages:
                await ctx.channel.send(page)

    @module.command(
        doc=CommandDocumentation(
            command="reload",
            doc=N_("Reload a bot module."),
            args=[
                ArgumentDocumentation(
                    name="module",
                    doc=N_(
                        "The module to reload. Must be a valid folder"
                        " name in the modules folder"
                    ),
                )
            ],
        ),
        name="reload",
    )
    @dcommands.is_owner()
    async def module_reload(self, ctx: ContextBase, module: str):
        try:
            self.bot.reload_module(module.lower())

            await ctx.channel.send(
                _("Module `{}` has been reloaded!", locale=ctx.locale).format(
                    module
                )
            )
        except dcommands.errors.ExtensionNotLoaded:
            await ctx.channel.send(
                _(
                    "Cannot reload module `{}`: Not loaded!",
                    locale=ctx.locale,
                ).format(module)
            )
        except dcommands.errors.ExtensionFailed as err:
            await ctx.channel.send(
                _(
                    "An error occured while trying to load module `{}`:",
                    ctx.locale,
                ).format(module)
            )

            pg: dcommands.Paginator = dcommands.Paginator(prefix="```python")

            for line in str(err).splitlines():
                pg.add_line(line)

            for page in pg.pages:
                await ctx.channel.send(page)

    @module.command(
        doc=CommandDocumentation(
            command="list", doc=N_("List all available modules.")
        ),
    )
    async def module_list(self, ctx: ContextBase):
        modules_dir: str = os.path.abspath(
            os.path.join(os.path.dirname(__file__), ".."),
        )

        modules: List[str] = [
            f.name
            for f in os.scandir(modules_dir)
            if f.is_dir() and f.name not in ["__pycache__"]
        ]

        table_rows: List[Tuple[str, str]] = []

        for m in modules:
            mod = importlib.import_module(f"belchan.modules.{m}")

            for o in mod.__dict__.values():
                if (
                    inspect.isclass(o)
                    and issubclass(o, Module)
                    and hasattr(o, "get_description")
                ):
                    desc = o.get_description().localize(locale=ctx.locale)

                    # Sometimes "desc" is Null.. I don't want to debug this, so
                    # this is the quickest fix I could find.
                    #
                    # TODO : find better solution.
                    if desc is not None:
                        table_rows.append((m, desc))

        pg: Paginator = Paginator()

        for row in tabulate(
            table_rows, tablefmt="plain", maxcolwidths=[None, 60]
        ).splitlines():
            pg.add_line(row)

        for page in pg.pages:
            await ctx.send(page)

    @staticmethod
    async def __configure_module(
        ctx: ContextBase,
        module: str,
        option: str,
        mode: ModeEnum,
        *,
        value: Union[
            discord.User,
            discord.Message,
            discord.TextChannel,
            discord.VoiceChannel,
            discord.CategoryChannel,
            discord.Emoji,
            discord.PartialEmoji,
            discord.Role,
            int,
            float,
            Duration,
            DateTime,
            str,
            list,
        ] = None,
    ):
        cfg: ModuleConfig = ctx.bot.get_module_config(ctx.guild.id, module)

        if mode not in ModeEnum:
            await ctx.channel.send(
                _(
                    "Invalid value for `mode`. Must be one of: {}",
                    locale=ctx.locale,
                ).format(", ".join([m for m in ModeEnum]))
            )

            await ctx.bot.show_help_for(ctx, ctx.command.name)

            return

        if cfg is None:
            # No module config for the module exists on this server. Load the
            # default template for the given module.
            try:
                cfg = ctx.bot._module_config_templates[module.lower()]
            except KeyError:
                await ctx.channel.send(
                    _(
                        "A module with the given name does not exist.",
                        locale=ctx.locale,
                    )
                )

                return

        if option is None:
            # No option is set, but module was given. Show a list of all
            # available options.
            cfg = await config.print_object(ctx, cfg)

            pg = dcommands.Paginator()

            for line in cfg.splitlines():
                pg.add_line(line)

            for page in pg.pages:
                await ctx.send(page)

            return

        if mode == "get":
            old_value: Any = None

            try:
                old_value = getattr(cfg, option)
            except AttributeError:
                await ctx.channel.send(
                    _(
                        "Invalid `option`. See below for a complete list of"
                        " available options.",
                        locale=ctx.locale,
                    )
                )

                return await ctx.bot.get_command("config")(ctx, module)

            if isinstance(old_value, str):
                old_value = old_value.replace("```", "\`\`\`")

            await ctx.channel.send(
                _("Value:", locale=ctx.locale) + f"\n```\n{old_value}\n```"
            )
        else:
            try:
                cfg = await config.update_object(ctx, cfg, option, mode, value)
            except AttributeError:
                await ctx.channel.send(
                    _(
                        "Invalid `option`. See below for a complete list of"
                        " available options.",
                        locale=ctx.locale,
                    )
                )

                return await ctx.bot.get_command("config")(ctx, module)
            except ValidationError as exc:
                for err in exc.errors():
                    await ctx.send(str(err["loc"]) + ": " + str(err["msg"]))

                    return

        new_value: Any = getattr(cfg, option)

        await ctx.bot.update_module_config(ctx.guild.id, module, cfg)

        if isinstance(new_value, str):
            new_value = value.replace("```", "\`\`\`")

        try:
            await ctx.channel.send(
                _("New Value:", locale=ctx.locale)
                + f"\n```\n"
                + await config.print_value(
                    ctx,
                    cfg.__fields__[option].type_,
                    new_value,
                )
                + "\n```"
            )
        except:
            pass

        with db_session():
            CommandLog.create(
                server_id=ctx.guild.id,
                command="config",
                user=ctx.author.id,
                time=datetime.now(),
                _args=json.dumps(
                    {
                        "mode": mode.value,
                        "value": await config.print_value(
                            ctx,
                            cfg.__fields__[option].type_,
                            new_value,
                        ),
                    }
                ),
            )

    @staticmethod
    async def __configure_server(
        ctx: ContextBase,
        option: str,
        mode: ModeEnum,
        value: Union[
            discord.User,
            discord.Message,
            discord.TextChannel,
            discord.VoiceChannel,
            discord.CategoryChannel,
            discord.Emoji,
            discord.PartialEmoji,
            discord.Role,
            int,
            float,
            Duration,
            DateTime,
            str,
            list,
        ] = None,
    ):
        cfg: ServerConfig = None

        try:
            cfg = ctx.bot.server_configs[ctx.guild.id]
        except KeyError:
            cfg = ServerConfig()

        if mode == "get":
            if option is None:
                cfg = await config.print_object(ctx, cfg)

                pg = dcommands.Paginator()

                for line in cfg.splitlines():
                    pg.add_line(line)

                for page in pg.pages:
                    await ctx.send(page)

                return
            try:
                old_value = getattr(cfg, option)
            except AttributeError:
                await ctx.channel.send(
                    _(
                        "Invalid `option`. See below for a complete list of"
                        " available options.",
                        locale=ctx.locale,
                    )
                )

                return await ctx.bot.get_command("config")(ctx, "server")

            if isinstance(old_value, str):
                old_value = old_value.replace("```", "\`\`\`")

            await ctx.channel.send(
                _("Value:", locale=ctx.locale) + f"\n```\n{old_value}\n```"
            )
        else:
            try:
                cfg = await config.update_object(ctx, cfg, option, mode, value)
            except AttributeError:
                await ctx.channel.send(
                    _(
                        "Invalid `option`. See below for a complete list of"
                        " available options.",
                        locale=ctx.locale,
                    )
                )

                return await ctx.bot.get_command("config")(ctx, "server")
            except ValidationError as err:
                for err in err.errors():
                    await ctx.send(f"{err.loc}: {err.exc}")

                    return

        await ctx.bot.update_server_config(ctx.guild.id, cfg)

        new_value: Any = getattr(cfg, option)

        if isinstance(new_value, str):
            new_value = new_value.replace("```", "\`\`\`")

        try:
            await ctx.channel.send(
                _("New Value:", locale=ctx.locale)
                + f"\n```\n"
                + await Config.print_value(
                    ctx,
                    cfg.__fields__[option].type_,
                    new_value,
                )
                + "\n```"
            )
        except:
            pass

        with db_session():
            CommandLog.create(
                server_id=ctx.guild.id,
                command="config",
                user=ctx.author.id,
                time=datetime.now(),
                _args=json.dumps(
                    {
                        "mode": mode.value,
                        "value": await config.print_value(
                            ctx,
                            cfg.__fields__[option].type_,
                            new_value,
                        ),
                    }
                ),
            )


def setup(bot: Application):
    ext = Config(bot)

    bot.add_module(ext)


def teardown(bot: Application):
    bot.remove_module(Config)
