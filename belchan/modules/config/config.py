from belchan.util.log import FieldInfo
from typing import (
    Any,
    Literal,
    TypeVar,
    Type,
    List,
    Tuple,
    Set,
    get_origin,
    get_args,
)

from tabulate import tabulate

from belchan.context import ContextBase
from belchan.models.config import ConfigBase
from belchan.models.discord import DiscordModel
from belchan.i18n import _

T = TypeVar("T")


async def update_object(
    ctx: ContextBase,
    cfg: T,
    option: str,
    mode: Literal["set", "unset", "add", "remove"],
    to_value: Any,
) -> T:
    new_value: Any = getattr(cfg, option)

    option_type: Type = cfg.__fields__[option].type_
    outer_type: Type = get_origin(cfg.__fields__[option].outer_type_)

    if to_value:
        if issubclass(option_type, DiscordModel):
            to_value = await option_type.convert(ctx, to_value)

    if mode == "set":
        new_value = to_value
    elif mode == "unset":
        if issubclass(outer_type, list):
            new_value = list()
        elif issubclass(outer_type, set):
            new_value = set()
        else:
            new_value = None
    elif mode == "add":
        if new_value is None:
            if outer_type:
                new_value = outer_type()
            else:
                new_value = option_type()

        if isinstance(new_value, list):
            if isinstance(to_value, list) or isinstance(to_value, set):
                new_value.extend(*to_value)
            else:
                new_value.append(to_value)
        elif isinstance(new_value, set):
            if isinstance(to_value, list) or isinstance(to_value, set):
                new_value.update(*to_value)
            else:
                new_value.add(to_value)
        else:
            new_value += to_value
    elif mode == "remove":
        if isinstance(new_value, list) or isinstance(new_value, set):
            if isinstance(to_value, list) or isinstance(to_value, set):
                for v in to_value:
                    new_value.remove(v)
            else:
                new_value.remove(to_value)
        else:
            new_value -= to_value

    setattr(cfg, option, new_value)

    cfg.validate()

    return cfg


async def print_object(ctx: ContextBase, cfg: ConfigBase) -> str:
    table_rows: List[Tuple[str, str, Any]] = []

    for field, info in cfg.__fields__.items():
        value = getattr(cfg, field)

        if isinstance(value, str):
            value = value.replace("```", "\`\`\`")

        table_rows.append(
            (
                field,
                get_type_name(info),
                await print_value(ctx, info.type_, value),
            )
        )

    return tabulate(
        table_rows,
        headers=(
            _("Option", locale=ctx.locale),
            _("Type", locale=ctx.locale),
            _("Value", locale=ctx.locale),
        ),
        maxcolwidths=[None, None, 60],
    )


async def print_value(ctx: ContextBase, type_: Type[Any], value: Any) -> str:
    if isinstance(value, list):
        ret_values: List[Any] = [
            await print_value(ctx, type_, v) for v in value
        ]

        return ", ".join(ret_values)
    elif isinstance(value, set):
        ret_values: Set[Any] = set(
            [await print_value(ctx, type_, v) for v in value]
        )

        return ", ".join(ret_values)

    if issubclass(type_, DiscordModel) and not isinstance(value, DiscordModel):
        return str(await type_.from_id(ctx, value))
    else:
        return str(value)


def get_type_name(info: FieldInfo) -> str:
    """Get the type name for the given field info."""
    if get_origin(info.outer_type_) is not None:
        return (
            str(get_origin(info.outer_type_).__name__)
            + "["
            + ", ".join([str(t.__name__) for t in get_args(info.outer_type_)])
            + "]"
        )
    else:
        return info.type_.__name__
