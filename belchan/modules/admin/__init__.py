from typing import List, Optional, Union, Set, Dict, Tuple

import datetime
from enum import Enum
from contextvars import ContextVar

import discord
import discord_argparse
import tabulate
from discord.ext import tasks

from belchan.application import Application
from belchan import commands as dcommands
from belchan.context import ContextBase
from belchan.converters import DateTime, Duration
from belchan.database import DoesNotExist, IntegrityError, db_session
from belchan.models.docs import (
    CommandDocumentation,
    ArgumentDocumentation,
    CommandExample,
    GroupDocumentation,
)
from belchan.models import Model, validator, constr, ValidationError, Field
from belchan.models.config import ModuleConfig
from belchan.models.database import RolePermissions
from belchan.models.discord import Role, TextChannel
from belchan.models.permissions import (
    InvalidPermission,
    ModulePermission,
    MissingBotPermission,
)
from belchan.modules import Module
from belchan.permissions import (
    require_any_permission,
    require_permission,
    require_enabled,
    member_has_permission,
)
from belchan.i18n import lazy_gettext as N_, _
from belchan.util import dialog
from belchan.util.log import logged_command
from belchan import converters

from .database import Warning, Ban, Mute
from . import commands as mod_commands


context: ContextVar = ContextVar("ctx")
"""Context(thread) local storage for the ctx variable. Needed for validators."""


class BotPermissionOperator(str, Enum):
    add: str = "+"
    remove: str = "-"


def permission_exists_validator(cls, value: str):
    ctx: ContextBase = context.get()

    if value not in {n.name for n in ctx.bot.permissions}:
        raise InvalidPermission(
            _("Invalid permission: '{permission}'", locale=ctx.locale).format(
                permission=value
            )
        )

    return value


class BotPermissionAction(Model, dcommands.Converter):
    op: BotPermissionOperator
    permission: constr(min_length=1)
    validate_permission_exists = validator("permission", allow_reuse=True)(
        permission_exists_validator
    )


prune_argument_converter = discord_argparse.ArgumentConverter(
    channel=discord_argparse.OptionalArgument(
        discord.TextChannel, default=None
    ),
    user=discord_argparse.OptionalArgument(
        Union[discord.Member, discord.User], default=None
    ),
    count=discord_argparse.OptionalArgument(int, default=899),
)


class AdminConfig(ModuleConfig):
    command_log_channel: Optional[TextChannel] = Field(
        None, description="The log channel for admin commands."
    )
    mute_role: Optional[Role] = Field(
        None, description="The role assigned to muted users."
    )

    ##########################
    # Bans
    ban_dm_title: Optional[str] = Field(
        "You have been banned from {server.name}.",
        description="Title row of ban DMs.",
    )
    ban_dm_content: Optional[str] = Field(
        "Reason:\n\n{reason}", description="Content of ban DMs."
    )

    ##########################
    # Warnings
    warning_duration: Optional[datetime.timedelta] = Field(
        None, description="How long warnings should be active (in days)."
    )
    warning_log_channel: Optional[TextChannel] = Field(
        None, description="The log channel for warnings and bans."
    )
    warning_dm_title: Optional[str] = Field(
        "You have received a warning on {server.name}.",
        description="Title row of warning DMs.",
    )
    warning_dm_content: Optional[str] = Field(
        "Reason:\n\n{reason}", description="Content of warning DMs."
    )
    warning_limit_active: Optional[int] = Field(
        None,
        description=(
            "How many active warnings are allowed before the bot advises a ban."
        ),
    )
    warning_limit_total: Optional[int] = Field(
        None,
        description=(
            "How many warnings are allowed in total before the bot advises"
            " a ban."
        ),
    )
    warning_limit_autoban: Optional[bool] = Field(
        False,
        description=(
            "If true, members will automatically be banned when they"
            " reach the warning limit."
        ),
    )


async def LogChannelResolver(ctx: ContextBase) -> Union[TextChannel, None]:
    cfg: AdminConfig = ctx.module_config

    if cfg.command_log_channel:
        return ctx.guild.get_channel(cfg.command_log_channel)
    elif ctx.server_config.log_channel:
        return ctx.guild.get_channel(cfg.command_log_channel)

    return None


async def DeletedMessageLogger(ctx: ContextBase, *args, **kwargs):
    channel = await LogChannelResolver(ctx)

    embed = discord.Embed(
        title=_(
            "`{user.display_name} ({user.id})` deleted"
            " `{message.author.display_name} ({message.author.id})`'s message"
        ).format(user=ctx.author, message=kwargs["message_id"])
    )

    await channel.send(embed=embed)


class Admin(
    Module,
    doc=N_("Server administration commands."),
    config=AdminConfig,
    permissions=[
        ModulePermission(name="delete", description=N_("Deleting messages.")),
        ModulePermission(
            name="mute", description=N_("Muting and unmuting members.")
        ),
        ModulePermission(
            name="warn",
            description=N_("Warning members and deleting warnings."),
        ),
        ModulePermission(
            name="ban", description=N_("Banning and unbanning members.")
        ),
        ModulePermission(
            name="member_roles",
            description=N_("Modifying member's roles."),
        ),
        ModulePermission(
            name="edit_roles", description=N_("Edit role permissions.")
        ),
        ModulePermission(name="delete", description=N_("Delete messages.")),
        ModulePermission(name="prune", description=N_("Nukes channels.")),
    ],
    database_tables=[Ban, Mute, Warning],
):
    def __init__(self, bot: Application):
        super().__init__(bot)

        self._admin_task_loop.start()

    def cog_unload(self):
        self._admin_task_loop.stop()

    @dcommands.command(
        doc=CommandDocumentation(
            command="ban",
            doc=N_("Ban a user from the server."),
            args=[
                ArgumentDocumentation(
                    name="user", doc=N_("The user that should be banned.")
                ),
                ArgumentDocumentation(
                    name="delete_message_days",
                    doc=N_(
                        "How many days of messages by the user should be "
                        "deleted. Max value is 7."
                    ),
                    optional=True,
                    default=0,
                ),
                ArgumentDocumentation(
                    name="until",
                    doc=N_(
                        "The duration the ban lasts or the date when"
                        " the ban should be lifted."
                    ),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="reason",
                    doc=N_(
                        "The reason for the ban. The banned user will"
                        " see this."
                    ),
                ),
            ],
        )
    )
    @dcommands.guild_only()
    @require_enabled("admin")
    @require_any_permission(["ban", discord.Permissions.ban_members])
    async def ban(
        self,
        ctx: ContextBase,
        user: Union[discord.Member, discord.User],
        delete_message_days: Optional[int] = 0,
        until: Optional[Union[Duration, DateTime]] = None,
        *,
        reason: str,
    ):
        cfg: AdminConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        if await dialog.confirmation(
            self.bot,
            ctx.channel,
            text=_(
                "Do you really want to ban `{user.display_name} ({user.id})`"
                " from this server?",
                ctx.locale,
            ).format(user=user),
            user=ctx.author,
        ):
            try:
                await mod_commands.ban(
                    app=ctx.bot,
                    config=cfg,
                    locale=ctx.locale,
                    server=ctx.guild,
                    user=user,
                    by=ctx.author,
                    until=until,
                    reason=reason,
                    delete_message_days=delete_message_days,
                )

                await ctx.channel.send(
                    _(
                        "User {user.display_name} ({user.id}) has been banned.",
                        ctx.locale,
                    ).format(user=user)
                )
            except MissingBotPermission as exc:
                await ctx.channel.send(
                    _(
                        "Bot missing required permission: {permission}.",
                        locale=ctx.locale,
                    ).format(permission=str(exc))
                )
        else:
            await ctx.channel.send(_("OK I did nothing.", ctx.locale))

    @dcommands.command(
        doc=CommandDocumentation(
            command="unban",
            doc=N_("Lift the ban of a user from the server."),
            args=[
                ArgumentDocumentation(
                    name="user",
                    doc=N_("The user that should be unbanned."),
                ),
                ArgumentDocumentation(
                    name="reason", doc=N_("The reason for the unban.")
                ),
            ],
        )
    )
    @dcommands.guild_only()
    @require_permission("ban", discord.Permissions.ban_members)
    async def unban(
        self,
        ctx: ContextBase,
        user: Union[discord.Member, discord.User],
        *,
        reason: Optional[str] = None,
    ):
        cfg: AdminConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        await mod_commands.unban(
            app=ctx.bot,
            config=cfg,
            locale=ctx.locale,
            server=ctx.guild,
            reason=reason,
        )

        await ctx.channel.send(
            _(
                "User {user.display_name} ({user.id}) has been unbanned.",
                ctx.locale,
            ).format(user=user)
        )

    @dcommands.command(
        doc=CommandDocumentation(
            command="mute",
            doc=N_("Mute a user on the server."),
            args=[
                ArgumentDocumentation(
                    name="user", doc=N_("The user that will be muted.")
                ),
                ArgumentDocumentation(
                    name="until",
                    doc=N_(
                        "The duration the mute lasts or date when the"
                        " mute will be lifted."
                    ),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="reason",
                    doc=N_(
                        "The reason for the mute. The banned user will"
                        " see this."
                    ),
                ),
            ],
            examples=[
                CommandExample(
                    command="mute SomeUser 5h",
                    result=N_("User `SomeUser` is muted for 5 hours."),
                ),
                CommandExample(
                    command="mute SomeUser 365d",
                    result=N_("User `SomeUser` is muted for 365 days."),
                ),
                CommandExample(
                    command="mute SomeUser 2030-01-01",
                    result=N_("User `SomeUser` is muted until 2030"),
                ),
                CommandExample(
                    command='mute "Some User With Spaces" 5m',
                    result=N_(
                        "User `Some User With Spaces` is muted for 5 minutes."
                    ),
                ),
            ],
        )
    )
    @dcommands.guild_only()
    @require_enabled("admin")
    @require_permission("mute", discord.Permissions.mute_members)
    async def mute(
        self,
        ctx: ContextBase,
        user: Union[discord.Member, discord.User],
        until: Optional[Union[Duration, DateTime]] = None,
        *,
        reason: str,
    ):
        cfg: AdminConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        try:
            await mod_commands.mute(
                app=ctx.bot,
                config=cfg,
                locale=ctx.locale,
                server=ctx.guild,
                user=user,
                reason=reason,
                until=until,
                by=ctx.author,
            )

            await ctx.channel.send(
                _(
                    "User {user.display_name} ({user.id}) has been muted.",
                    ctx.locale,
                ).format(user=user)
            )
        except IntegrityError:
            await ctx.channel.send(
                _(
                    "User {user.display_name} ({user.id}) is already muted.",
                    ctx.locale,
                ).format(user=user)
            )

    @dcommands.command(
        doc=CommandDocumentation(
            command="unmute", doc=N_("Unmute a user on the server.")
        )
    )
    @dcommands.guild_only()
    @require_enabled("admin")
    @require_permission("mute", discord.Permissions.mute_members)
    async def unmute(
        self,
        ctx: ContextBase,
        user: Union[discord.Member, discord.User],
        *,
        reason: str,
    ):
        cfg: AdminConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        await mod_commands.unmute(
            app=ctx.bot,
            config=cfg,
            locale=ctx.locale,
            server=ctx.guild,
            user=user,
            reason=reason,
            by=ctx.author,
        )

        await ctx.channel.send(
            _(
                "User {user.display_name} ({user.id}) has been unmuted.",
                ctx.locale,
            ).format(user=user)
        )

    @dcommands.command(
        doc=CommandDocumentation(
            command="warn",
            doc=N_("Give a warning to a user on the server."),
            args=[
                ArgumentDocumentation(
                    name="user",
                    doc=N_("The user that will receive the warning."),
                ),
                ArgumentDocumentation(
                    name="reason",
                    doc=N_(
                        "The reason for the warning. The warned "
                        " user will see this."
                    ),
                ),
            ],
        )
    )
    @dcommands.guild_only()
    @require_enabled("admin")
    @require_permission("warn")
    async def warn(
        self,
        ctx: ContextBase,
        user: Union[discord.Member, discord.User],
        *,
        reason: str,
    ):
        cfg: AdminConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        try:
            await mod_commands.warn(
                app=ctx.bot,
                config=cfg,
                locale=ctx.locale,
                server=ctx.guild,
                user=user,
                reason=reason,
                by=ctx.author,
            )

            await ctx.channel.send(
                _(
                    "User {user.display_name} ({user.id}) has been issued a"
                    " warning.",
                    ctx.locale,
                ).format(user=user)
            )
        except MissingBotPermission as exc:
            await ctx.send(
                _(
                    "Bot missing required permission: {permission}.",
                    locale=ctx.locale,
                ).format(permission=str(exc))
            )

    @dcommands.command(
        doc=CommandDocumentation(
            command="warnlist",
            doc=N_("List warnings for a user."),
            args=[
                ArgumentDocumentation(
                    name="user",
                    doc=N_(
                        "The user to list the warnings for. Defaults"
                        " to the user who invoked the command."
                    ),
                    optional=True,
                )
            ],
        )
    )
    @dcommands.guild_only()
    @require_enabled("admin")
    async def warnlist(
        self,
        ctx: ContextBase,
        user: Union[discord.Member, discord.User] = None,
    ):
        if user is None:
            user = ctx.author

        perm: bool = await member_has_permission(ctx.author, "warn")

        if user.id != ctx.author.id and not perm:
            await ctx.channel.send(_("Missing permissions.", ctx.locale))

            return

        warnings: List[Warning] = Warning.select().where(
            Warning.server_id == ctx.guild.id,
            Warning.user_id == ctx.author.id,
        )

        if len(warnings) == 0:
            await ctx.channel.send(_("No warnings.", ctx.locale))

            return

        if perm:
            warnlist = tabulate.tabulate(
                [
                    [
                        e.id,
                        e.reason.replace("```", ""),
                        e.date.replace(microsecond=0),
                        await converters.to_user_or_member(
                            ctx.bot, e.by, ctx.guild
                        ),
                    ]
                    for e in warnings
                ],
                [
                    "ID",
                    _("Reason", ctx.locale),
                    _("Date", ctx.locale),
                    _("By", ctx.locale),
                ],
                maxcolwidths=[None, None, None, 50],
            )

            pg: dcommands.Paginator = dcommands.Paginator()

            for line in warnlist.splitlines():
                pg.add_line(line)

            for page in pg.pages:
                await ctx.channel.send(page)
        else:
            warnlist = tabulate.tabulate(
                [
                    (
                        e.date.replace(microsecond=0),
                        e.reason.replace("```", ""),
                    )
                    for e in warnings
                ],
                (_("Date", ctx.locale), _("Reason", ctx.locale)),
                maxcolwidths=[None, 60],
            )

            pg: dcommands.Paginator = dcommands.Paginator()

            for line in warnlist.splitlines():
                pg.add_line(line)

            for page in pg.pages:
                await ctx.channel.send(page)

    @dcommands.command(
        doc=CommandDocumentation(
            command="delete_warning",
            aliases=["warndel", "delwarn"],
            doc=N_("Delete a previously given warning."),
            args=[
                ArgumentDocumentation(
                    name="warning",
                    doc=N_("The ID of the warning that should be deleted."),
                ),
                ArgumentDocumentation(
                    name="reason",
                    doc=N_("The reason for deleting the warning."),
                    optional=True,
                    variadic=True,
                ),
            ],
        ),
        aliases=["warndel", "delwarn"],
    )
    @dcommands.guild_only()
    @require_enabled("admin")
    @require_permission("warn")
    async def delete_warning(
        self, ctx: ContextBase, warning: int, *, reason: str = None
    ):
        cfg: AdminConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        warning_entry: Warning = None

        try:
            warning_entry = Warning.get(Warning.id == warning)
        except DoesNotExist:
            await ctx.channel.send(_("No such warning exists.", ctx.locale))

            return

        if warning_entry.server_id != ctx.guild.id:
            await ctx.channel.send(
                _("This command does not work across servers.", ctx.locale)
            )

            return

        await mod_commands.delete_warning(
            app=ctx.bot,
            config=cfg,
            locale=ctx.locale,
            server=ctx.guild,
            warning_id=warning,
            reason=reason,
            by=ctx.author,
        )

        await ctx.channel.send(
            _("Warning {} has been deleted.", ctx.locale).format(warning)
        )

    @dcommands.command(
        doc=CommandDocumentation(
            command="role_add",
            aliases=["add_role"],
            doc=N_("Add a role to a member."),
            args=[
                ArgumentDocumentation(
                    name="member",
                    doc=N_("The member to assign the role to."),
                ),
                ArgumentDocumentation(
                    name="role", doc=N_("The role to assign.")
                ),
            ],
        ),
        aliases=["add_role"],
    )
    @require_enabled("admin")
    @require_permission("member_roles")
    @logged_command()
    async def role_add(
        self,
        ctx: ContextBase,
        member: discord.Member,
        role: discord.Role,
    ):
        await mod_commands.role_add(
            ctx.bot, server=ctx.guild, user=member, role=role, by=ctx.author
        )

        await ctx.channel.send(
            _(
                "Added role `{role.name}` to `{user.display_name}"
                " ({user.id})`.",
                locale=ctx.locale,
            ).format(role=role, user=member)
        )

    @dcommands.command(
        doc=CommandDocumentation(
            command="role_remove",
            aliases=["remove_role"],
            doc=N_("Remove a role from a member."),
            args=[
                ArgumentDocumentation(
                    name="member",
                    doc=N_("The member to remove the role from."),
                ),
                ArgumentDocumentation(
                    name="role", doc=N_("The role remove assign.")
                ),
            ],
        ),
        aliases=["remove_role"],
    )
    @require_enabled("admin")
    @require_permission("member_roles")
    @logged_command()
    async def role_remove(
        self,
        ctx: ContextBase,
        member: discord.Member,
        role: discord.Role,
    ):
        await mod_commands.role_add(
            ctx.bot, server=ctx.guild, user=member, role=role, by=ctx.author
        )

        await ctx.channel.send(
            _(
                "Removed role `{role.name}` from `{user.display_name}"
                " ({user.id})`.",
                locale=ctx.locale,
            ).format(role=role, user=member)
        )

    @dcommands.group(
        doc=GroupDocumentation(
            command="permissions",
            doc=N_("Permission management."),
            aliases=["perms"],
        ),
        name="permissions",
        aliases=["perms"],
    )
    @require_any_permission(
        [
            discord.Permissions.administrator,
            discord.Permissions.manage_permissions,
            "server_roles",
        ],
        guild=True,
    )
    async def permissions(self, ctx: ContextBase):
        if not ctx.invoked_subcommand:
            for page in (await self.permissions.make_help(ctx)).pages:
                await ctx.send(page)

    @permissions.command(
        doc=CommandDocumentation(
            command="help",
            doc=N_("List all permissions and what they are for."),
        ),
        name="help",
    )
    @require_any_permission(
        [
            discord.Permissions.administrator,
            discord.Permissions.manage_permissions,
            "server_roles",
        ],
        guild=True,
    )
    async def permissions_help(self, ctx: ContextBase):
        table_rows: List[Tuple[str, str]] = []

        pg: dcommands.Paginator = dcommands.Paginator(prefix="```apache")

        for perm in ctx.bot.permissions:
            table_rows.append(
                (perm.name, perm.description.localize(ctx.locale))
            )

        for row in tabulate.tabulate(
            table_rows, tablefmt="plain", maxcolwidths=[None, 60]
        ).splitlines():
            pg.add_line(row)

        for page in pg.pages:
            await ctx.channel.send(page)

    @permissions.command(
        doc=CommandDocumentation(
            command="edit",
            doc=N_("Edit a role's bot permissions."),
            args=[
                ArgumentDocumentation(name="role", doc=N_("The role to edit.")),
                ArgumentDocumentation(
                    name="permission",
                    doc=N_("The permission to add (+) or remove (-)."),
                    variadic=True,
                ),
            ],
        ),
        aliases=["editperms"],
        name="edit",
    )
    @require_any_permission(
        [
            discord.Permissions.administrator,
            discord.Permissions.manage_permissions,
            "server_roles",
        ],
        guild=True,
    )
    @logged_command()
    @db_session
    async def permsissions_edit(
        self,
        ctx: ContextBase,
        role: discord.Role,
        *,
        permissions: str,
    ):
        parsed_permissions: List[BotPermissionOperator] = []

        reset_token = context.set(ctx)

        entry = RolePermissions.get_or_create(
            server_id=ctx.guild.id, role_id=role.id
        )[0]

        entry_permissions: Set[str] = set(entry.permissions)

        for perm in permissions.split():
            try:
                parsed_permissions.append(
                    BotPermissionAction(
                        op=BotPermissionOperator(perm[0]), permission=perm[1:]
                    )
                )
            except InvalidPermission as exc:
                await ctx.channel.send(str(exc))
            except ValueError:
                await ctx.channel.send(
                    _(
                        "Permissions need to be prefixed with `+` or `-`.",
                        locale=ctx.locale,
                    ).format(op=perm[0])
                )

                return
            except ValidationError:
                context.reset(reset_token)

                await ctx.channel.send(
                    _(
                        "Invalid permission: `{permission}`.",
                        locale=ctx.locale,
                    ).format(permission=perm[1:])
                )

                return

        for perm in parsed_permissions:
            if perm.op == BotPermissionOperator.add:
                entry_permissions.add(perm.permission)
            else:
                entry_permissions.remove(perm.permission)

        context.reset(reset_token)

        entry.permissions = set(sorted(entry_permissions))
        entry.save()

        await self.permissions_view(ctx, role)

    @permissions.command(
        CommandDocumentation(
            command="view",
            aliases=["listperms"],
            doc=N_("Lists all permissions of the given target."),
            args=[
                ArgumentDocumentation(
                    name="target",
                    doc=N_("The user or role to view the permissions of."),
                )
            ],
        ),
        aliases=["viewperms"],
        name="view",
    )
    @require_any_permission(
        ["edit_roles", discord.Permissions.manage_roles], guild=True
    )
    async def permissions_view(
        self,
        ctx: ContextBase,
        target: Union[discord.Member, discord.Role],
    ):
        target_permissions: Set[str] = set()
        all_permissions: Dict[str, str] = dict(
            {p.name: p.description for p in ctx.bot.permissions}
        )

        if isinstance(target, discord.Member):
            for role in target.roles:
                try:
                    entry = RolePermissions.get(
                        RolePermissions.role_id == role.id
                    )

                    target_permissions.update(entry.permissions)
                except DoesNotExist:
                    pass
        else:
            try:
                entry = RolePermissions.get(
                    RolePermissions.role_id == target.id
                )

                target_permissions = entry.permissions
            except DoesNotExist:
                pass

        table_data = []

        for perm in sorted(all_permissions):
            table_data.append(
                (
                    "+" if perm in target_permissions else "-",
                    perm,
                    all_permissions[perm],
                )
            )

        pg: dcommands.Paginator = dcommands.Paginator(prefix="```diff")

        for line in tabulate.tabulate(
            table_data,
            (
                "",
                _("Permission", locale=ctx.locale),
                _("Description", locale=ctx.locale),
            ),
            tablefmt="plain",
            maxcolwidths=[None, 60],
        ).splitlines():
            pg.add_line(line)

        for page in pg.pages:
            await ctx.channel.send(page)

    @dcommands.command(
        doc=CommandDocumentation(
            command="delete", doc=N_("Delete a message."), args=[]
        )
    )
    @require_any_permission(["delete", discord.Permissions.manage_messages])
    async def delete(
        self,
        ctx: ContextBase,
        channel: Optional[discord.TextChannel],
        message_id: int,
        *,
        reason: str,
    ):
        if channel is None:
            channel = ctx.channel

        try:
            await mod_commands.delete(
                ctx.bot, ctx.guild, ctx.author, channel, message_id, reason
            )
        except Exception as exc:
            await ctx.send(exc)

    @dcommands.command(
        doc=CommandDocumentation(
            command="prune",
            doc=N_("Bulk delete messages from a channel."),
            args=[
                ArgumentDocumentation(
                    name="channel",
                    doc=N_("The channel to prune."),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="user",
                    doc=N_(
                        "The command will only delete messages of this user,"
                        " if set."
                    ),
                    optional=True,
                ),
                ArgumentDocumentation(
                    name="count",
                    doc=N_("Up to how many messages should be deleted."),
                    optional=True,
                    default=899,
                ),
            ],
            examples=[
                CommandExample(
                    command="prune channel=#off-topic",
                    result=N_("Deletes 899 messages in channel `#off-topic`."),
                ),
                CommandExample(
                    command='prune user="Some User"',
                    result=N_(
                        "Searches the current channel for messages by"
                        " `Some User` in the last 899 messages and deletes"
                        " them."
                    ),
                ),
                CommandExample(
                    command="prune count=10",
                    result=N_("Deletes 10 messages in the current channel."),
                ),
                CommandExample(
                    command='prune channel=#off-topic user="Some User" count=100',
                    result=N_(
                        "Searches the channel `#off-topic` for messages by"
                        " the user named `Some User` in the last 100 messages"
                        " and deletes them."
                    ),
                ),
            ],
            aliases=["clear"],
        ),
        aliases=["clear"],
    )
    @require_any_permission(["delete", discord.Permissions.manage_messages])
    async def prune(
        self,
        ctx: ContextBase,
        *,
        params: prune_argument_converter = prune_argument_converter.defaults(),
    ):
        channel: Optional[discord.TextChannel] = params.get(
            "channel", ctx.channel
        )
        user: Optional[Union[discord.Member, discord.User]] = params.get(
            "user", None
        )
        count: int = params.get("count", None)

        if user:
            if not await dialog.confirmation(
                self.bot,
                ctx.channel,
                text=_(
                    "Do you really want to delete {count} messages by"
                    " {user.mention} in {channel.mention}?",
                    locale=ctx.locale,
                ).format(user=user, count=count, channel=channel),
                user=ctx.author,
            ):
                await ctx.channel.send(
                    _("OK, I did nothing.", locale=ctx.locale)
                )

                return
        else:
            if not await dialog.confirmation(
                self.bot,
                ctx.channel,
                text=_(
                    "Do you really want to delete {count} messages in channel"
                    " {channel.mention}?",
                    locale=ctx.locale,
                ).format(count=count, channel=channel),
                user=ctx.author,
            ):
                await ctx.channel.send(
                    _("OK, I did nothing.", locale=ctx.locale)
                )

                return

        if user is not None:
            deleted = (
                len(
                    await channel.purge(
                        limit=count + 2,
                        check=lambda message: message.author == user,
                    )
                )
                - 2
            )
        else:
            deleted = len(await channel.purge(limit=count + 2)) - 2

        await ctx.channel.send(
            _(
                "Deleted {count} messages in channel {channel.mention}.",
                locale=ctx.locale,
            ).format(count=deleted, channel=channel)
        )

    ###########################################################################
    # Event handlers

    @dcommands.listener()
    async def on_member_join(self, member: discord.Member):
        cfg: AdminConfig = self.bot.get_module_config(
            member.guild.id, self.qualified_name
        )

        now = datetime.datetime.now()

        mute_entry = database.Mute.get_or_none(
            database.Mute.user_id == member.id,
            database.Mute.server_id == member.guild.id,
        )

        if mute_entry is not None:
            if mute_entry.until > now:
                role = cfg.mute_role
                member.add_roles(role)

    @tasks.loop(minutes=1.0)
    async def _admin_task_loop(self):
        await self._unmute_task()

    @_admin_task_loop.before_loop
    async def _admin_task_loop_setup(self):
        # Wait until the bot is loaded.
        await self.bot.wait_until_ready()

    async def _unmute_task(self):
        now = datetime.datetime.now()
        server = None
        config = None

        mutes = (
            database.Mute.select()
            .where(database.Mute.until < now)
            .order_by(database.Mute.server_id)
        )

        for entry in mutes:
            if server is None:
                server = self.bot.get_guild(entry.server_id)
                config = self.bot.get_module_config(
                    entry.server_id, self.qualified_name
                )
            elif server.id != entry.server_id:
                server = self.bot.get_guild(entry.server_id)
                config = self.bot.get_module_config(
                    entry.server_id, self.qualified_name
                )

            if server is not None:
                user = await server.fetch_member(entry.user_id)
                role = server.get_role(config.mute_role)

                if role is None:
                    self.bot.logger.warning(
                        f"[Server: {server.name} ({server.id})]: No mute role"
                        "defined."
                    )
                    continue

                if user is not None:
                    await user.remove_roles(role)

                    self.bot.logger.debug(
                        f"[Server: {server.name} ({server.id})]: Unmuted"
                        f"{user.display_name} ({user.id})."
                        "Reason: Mute duration."
                    )
                else:
                    self.bot.logger.warning(
                        f"[Server: {server.name} ({server.id})]: User ID:"
                        f"{entry.user_id} not found."
                    )
            else:
                self.bot.logger.warning(
                    f"Could not find server ID:{entry.server_id} for entry:"
                    f"{entry.id}."
                )

            entry.delete_instance()


def setup(bot: Application):
    ext = Admin(bot)

    bot.add_module(ext)


def teardown(bot: Application):
    bot.remove_module(Admin.qualified_name)
