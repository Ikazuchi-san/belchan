from typing import Optional

import datetime

from belchan.models.database import (
    BooleanField,
    DateTimeField,
    IntegerField,
    Model,
    TextField,
)


class Ban(Model):
    user_id: int = IntegerField()
    server_id: int = IntegerField()
    by: int = IntegerField()
    reason: str = TextField()
    date: datetime.datetime = DateTimeField(default=datetime.datetime.now())
    until: Optional[datetime.datetime] = DateTimeField(null=True)

    class Meta:
        indexes = ((("user_id", "server_id"), True),)


class AutoBan(Model):
    user_id: int = IntegerField()
    server_id: int = IntegerField()
    by: int = IntegerField()
    reason: str = TextField()
    triggered: bool = BooleanField()

    class Meta:
        indexes = ((("user_id", "server_id"), True),)


class Warning(Model):
    id: int = IntegerField(primary_key=True)
    user_id: int = IntegerField()
    server_id: int = IntegerField()
    by: int = IntegerField()
    reason: str = TextField()
    date: datetime.datetime = DateTimeField(default=datetime.datetime.now())


class Mute(Model):
    id: int = IntegerField(primary_key=True)
    user_id: int = IntegerField()
    server_id: int = IntegerField()
    by: int = IntegerField()
    reason: str = TextField()
    date: datetime.datetime = DateTimeField(default=datetime.datetime.now())
    until: Optional[datetime.datetime] = DateTimeField(null=True)

    class Meta:
        indexes = ((("user_id", "server_id"), True),)
