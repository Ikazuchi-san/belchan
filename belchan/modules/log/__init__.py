from typing import Optional

import datetime

import discord

from belchan.application import Application
from belchan.models import Field
from belchan.models.discord import TextChannel
from belchan.models.config import ModuleConfig, ServerConfig
from belchan.modules import Module
from belchan.context import ContextBase
from belchan.i18n import N_, _
from belchan import commands as dcommands


class LogConfig(ModuleConfig):
    log_channel: Optional[TextChannel] = Field(
        None,
        description="The channel where the log entries are written to.",
    )

    log_nitro_boosts: Optional[bool] = Field(
        False,
        description="Whether to log nitro boosters.",
    )
    nitro_log_channel: Optional[TextChannel] = Field(
        None,
        description=(
            "The channel where the nitro boost notifications are written to."
        ),
    )


class Log(Module, doc=N_("User message logger."), config=LogConfig):
    ###########################################################################
    # Event handlers

    @dcommands.Cog.listener()
    async def on_message_delete(self, message: discord.Message):
        if message.guild is None:
            return

        if message.author.id == self.bot.user.id:
            return

        m_cfg: LogConfig = self.bot.get_module_config(
            message.guild.id, self.qualified_name
        )

        if not m_cfg.enabled:
            return

        s_cfg: ServerConfig = self.bot.get_server_config(message.guild.id)

        log_channel_id = await Log.get_log_channel(m_cfg, s_cfg)

        if log_channel_id is None:
            return

        locale: str = self.bot.get_locale(message.guild.id)
        log_channel: TextChannel = message.guild.get_channel(log_channel_id)
        embed: discord.Embed = discord.Embed(
            title=_("A message has been deleted.", locale=locale),
            description=message.clean_content,
        )

        embed.add_field(
            name=_("Author", locale=locale),
            value=f"{message.author.mention}",
            inline=True,
        )
        embed.add_field(
            name=_("Channel", locale=locale),
            value=f"{message.channel.mention}",
            inline=True,
        )
        embed.add_field(
            name=_("Date Posted", locale=locale),
            value=f"{message.created_at.replace(microsecond=0)}",
            inline=True,
        )
        embed.add_field(
            name=_("Date Deleted", locale=locale),
            value=datetime.datetime.now().replace(microsecond=0),
            inline=True,
        )

        await log_channel.send(embed=embed)

    @dcommands.Cog.listener()
    async def on_message_edit(
        self, before: discord.Message, after: discord.Message
    ):
        if after.guild is None:
            return

        if before.author.id == self.bot.user.id:
            return

        m_cfg: LogConfig = self.bot.get_module_config(
            before.guild.id, self.qualified_name
        )

        if not m_cfg.enabled:
            return

        s_cfg: ServerConfig = self.bot.get_server_config(before.guild.id)

        log_channel_id = await Log.get_log_channel(m_cfg, s_cfg)

        if log_channel_id is None:
            return

        locale: str = self.bot.get_locale(before.guild.id)
        log_channel: TextChannel = before.guild.get_channel(log_channel_id)
        embed: discord.Embed = discord.Embed(
            title=_("A message has been edited.", locale=locale),
            url=after.jump_url,
            description="**"
            + _("Old content:", locale=locale)
            + "**"
            + "\n"
            + before.clean_content,
        )
        embed.add_field(
            name=_("Author", locale=locale), value=after.author.mention
        )
        embed.add_field(
            name=_("Channel", locale=locale), value=after.channel.mention
        )
        embed.add_field(
            name=_("Date Posted", locale=locale),
            value=before.created_at.replace(microsecond=0),
        )
        embed.add_field(
            name=_("Date Edited", locale=locale),
            value=datetime.datetime.now().replace(microsecond=0),
        )

        await log_channel.send(embed=embed)

    @staticmethod
    async def get_log_channel(
        m_cfg: LogConfig,
        s_cfg: ServerConfig,
    ) -> Optional[int]:
        if m_cfg.log_channel is None:
            if s_cfg.log_channel:
                return s_cfg.log_channel
        else:
            return m_cfg.log_channel

        return None


def setup(bot: Application):
    bot.add_module(Log(bot))


def teardown(bot: Application):
    bot.remove_module(Log.qualified_name)
