from __future__ import annotations
from typing import Optional, TYPE_CHECKING, List, Union

import discord

from werkzeug.datastructures import FileStorage

from belchan import converters


if TYPE_CHECKING:
    from belchan.application import Application
    from . import GeneralConfig


async def say(
    app: Application,
    server: Union[discord.Guild, int],
    channel: Union[discord.TextChannel, int],
    text: Optional[str] = None,
    files: Optional[List[FileStorage]] = [],
):
    """Sends ``text`` to ``channel`` with ``files`` as attachments.

    Params:
        channel: The channel to send the message to.
        text: The text to send to the channel.
        files: A list of files to attach to the message.
    """
    server: discord.Guild = await converters.to_server(app, server)
    channel: discord.TextChannel = await converters.to_channel(
        app, server, channel
    )

    await channel.send(
        content=text,
        files=[discord.File(f.stream, f.filename) for f in files],
    )


async def edit(
    app: Application,
    channel: discord.TextChannel,
    message: Union[discord.Message, int],
    new_text: str,
):
    """Edit the given message ID in the given channel.

    Params:
        channel: The channel to search and edit the message in.
        message: The ID of the message to edit.
        new_text: The new text the message should have.
    """

    message = await channel.fetch_message(message)

    if isinstance(message, discord.Message):
        await message.edit(content=new_text)
    else:
        raise discord.NotFound(
            response=404, message="Message ID does not exist in channel."
        )


async def delete(
    app: Application,
    channel: discord.TextChannel,
    message: int,
    cfg: Optional[GeneralConfig] = None,
):
    pass
