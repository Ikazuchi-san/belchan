from typing import Union, Optional, List

import random
import datetime

import discord

from belchan.application import Application
from belchan.models.config import ModuleConfig
from belchan.models.docs import (
    CommandDocumentation,
    ArgumentDocumentation,
    CommandExample,
)
from belchan.fs import Bundle
from belchan.models.permissions import ModulePermission
from belchan.modules import Module
from belchan.permissions import require_permission, require_enabled
from belchan.i18n import _, lazy_gettext as N_
from belchan.context import ContextBase
from belchan import commands as dcommands
from belchan.models.base import Field
from belchan.converters import Duration, DateTime


class FunConfig(ModuleConfig):
    fake_ban_message: Optional[str] = None


class Fun(
    Module,
    doc=N_("Games and other fun commands."),
    config=FunConfig,
    permissions=[
        ModulePermission(
            name="manage_custom_reactions",
            description=N_("Edit custom reactions."),
        ),
        ModulePermission(
            name="slap",
            description=N_("Slap other members."),
        ),
        ModulePermission(
            name="fakeban",
            description=N_("Send fake-ban messages."),
        ),
        ModulePermission(
            name="hug",
            description=N_("Hug other members."),
        ),
    ],
):
    @dcommands.command(
        doc=CommandDocumentation(
            command="roll",
            doc=N_(
                "Return a random value from a list, or a random number from 1"
                " to the given number."
            ),
            args=[
                ArgumentDocumentation(
                    name="value",
                    doc=N_(
                        "A list of choices or the number of sides of the dice."
                    ),
                    default=6,
                    optional=True,
                    variadic=True,
                )
            ],
            examples=[
                CommandExample(
                    command="roll 6",
                    result=N_("Random number between and including 1 to 6."),
                ),
                CommandExample(
                    command='roll something "some other thing"',
                    result=N_("Either: `something` or `some other thing`."),
                ),
            ],
        )
    )
    @require_enabled("fun")
    async def roll(
        self,
        ctx: ContextBase,
        choices: dcommands.Greedy[Union[int, str]] = [6],
    ):
        if len(choices) > 1:
            await ctx.channel.send(
                _("The result is: `{result}`", locale=ctx.locale).format(
                    result=discord.utils.escape_mentions(random.choice(choices))
                )
            )
        else:
            if isinstance(choices[0], int):
                await ctx.channel.send(
                    _("The result is: `{result}`", locale=ctx.locale).format(
                        result=random.randrange(1, choices[0])
                    )
                )
            else:
                await ctx.channel.send(
                    _(
                        "When using the roll command with only 1 value, the"
                        " first value needs to be a number.",
                        locale=ctx.locale,
                    )
                )

    @dcommands.command(
        doc=CommandDocumentation(
            command="hug",
            doc=N_("Hug another user."),
            args=[
                ArgumentDocumentation(
                    name="user",
                    doc=N_("The user you want to hug."),
                    optional=True,
                )
            ],
        )
    )
    @require_permission("hug")
    async def hug(
        self, ctx: ContextBase, user: Optional[discord.Member] = None
    ):
        if user is None:
            user = ctx.author

    @dcommands.command(
        doc=CommandDocumentation(
            command="fakeban",
            doc=N_("Send a fake ban-message to a channel"),
            args=[
                ArgumentDocumentation(
                    name="channel",
                    doc=N_("The channel where the message should be sent."),
                ),
                ArgumentDocumentation(
                    name="user",
                    doc=N_("The user who will be fake-banned."),
                ),
                ArgumentDocumentation(
                    name="reason",
                    doc=N_("The ban-reason in the ban message."),
                ),
            ],
        )
    )
    @require_permission("fakeban")
    async def fakeban(
        self,
        ctx: ContextBase,
        channel: discord.TextChannel,
        user: discord.Member,
        duration: Union[DateTime, Duration] = None,
        *,
        reason: str = None,
    ):
        if len(reason) == 0:
            reason = "Behaviour"

        until = ""

        if duration is None:
            until = _("Permanent", locale=ctx.locale)
        else:
            if isinstance(duration, datetime.timedelta):
                # Ignore the linter. isinstance is neccessary here!
                # DateTime and Duration are converters for datetime and
                # timedelta respectively.
                until = (datetime.now() + duration).replace(microseconds=0)
            else:
                until = until.replace(microsecond=0)

        log = discord.Embed(color=0xFF0000)

        log.title = ctx.module_config().fake_ban_message

        if log.title is None:
            log.title = _(
                "{user.display_name} ({user.id}) just felt the banhammer.",
                locale=ctx.locale,
            ).format(user=user)
        else:
            log.title = log.title.format(user=user)

        log.description = reason
        log.add_field(name="By", value=f"{ctx.author.name}")
        log.add_field(name="Until", value=until)

        await channel.send(embed=log)


def setup(bot: Application):
    bot.add_module(Fun(bot))


def teardown(bot: Application):
    bot.remove_module(Fun.qualified_name)
