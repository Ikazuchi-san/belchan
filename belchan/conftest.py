import random
import asyncio
from pathlib import Path
from collections import namedtuple

import pytest
import discord.ext.test as dpytest
from discord.ext import commands
from pyfakefs.fake_filesystem import FakeFilesystem
from pyfakefs.fake_filesystem_unittest import Patcher

from .application import Application, make_application
from .config import Config
from .models import config
from . import permissions


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def mock_bot(
    mock_config: Config,
    monkeypatch,
    event_loop: asyncio.BaseEventLoop,
    guilds=2,
    channels=15,
    members=300,
) -> Application:
    bot = make_application(commands.Bot)(mock_config, loop=event_loop)

    async def mock_true(*args, **kwargs) -> bool:
        return True

    # Overwrite some permissions functions so all functions can be tested.
    monkeypatch.setattr(permissions, "member_has_permission", mock_true)
    monkeypatch.setattr(permissions, "require_enabled", mock_true)

    dpytest.configure(
        bot, num_guilds=guilds, num_channels=channels, num_members=members
    )

    yield bot


@pytest.fixture(autouse=True, scope="session")
def mock_config(modules=None):
    Config._Config__singleton = Config(
        name="belchan-test",
        instance_path=str(Path(__file__).parent / "instance"),
        secret_key="",
        discord=config.DiscordConfig(
            bot_token="",
            client_id=0,
            client_secret="",
            redirect_uri="",
        ),
        database=config.DatabaseConfig(driver="sqlite", filename=":memory:"),
        web=config.WebConfig(
            base_url="",
        ),
        modules=[
            str(Path(f).stem)
            for f in (Path(__file__).parent / "modules").iterdir()
            if f.is_dir() and f.stem != "__pycache__"
        ],
        owners=[],
        debug=True,
    )

    return Config._Config__singleton


@pytest.fixture(scope="function")
def mock_fs() -> FakeFilesystem:
    patcher = Patcher(allow_root_user=False)
    patcher.setUp()

    yield patcher.fs

    patcher.tearDown()


# region Dpy Mocks
@pytest.fixture()
def guild_factory():
    mock_guild = namedtuple("Guild", "id members")

    class GuildFactory:
        def get(self):
            return mock_guild(random.randint(1, 999999999), [])

    return GuildFactory()


@pytest.fixture()
def empty_guild(guild_factory):
    return guild_factory.get()


@pytest.fixture(scope="module")
def empty_channel():
    mock_channel = namedtuple("Channel", "id")
    return mock_channel(random.randint(1, 999999999))


@pytest.fixture(scope="module")
def empty_role():
    mock_role = namedtuple("Role", "id")
    return mock_role(random.randint(1, 999999999))


@pytest.fixture()
def member_factory(guild_factory):
    mock_member = namedtuple("Member", "id guild display_name")

    class MemberFactory:
        def get(self):
            return mock_member(
                random.randint(1, 999999999),
                guild_factory.get(),
                "Testing_Name",
            )

    return MemberFactory()


@pytest.fixture()
def empty_member(member_factory):
    return member_factory.get()


@pytest.fixture()
def user_factory():
    mock_user = namedtuple("User", "id")

    class UserFactory:
        def get(self):
            return mock_user(random.randint(1, 999999999))

    return UserFactory()


@pytest.fixture()
def empty_user(user_factory):
    return user_factory.get()


@pytest.fixture(scope="module")
def empty_message():
    mock_msg = namedtuple("Message", "content")
    return mock_msg("No content.")


@pytest.fixture()
def ctx(empty_member, empty_channel, bot):
    mock_ctx = namedtuple("Context", "author guild channel message bot")
    return mock_ctx(
        empty_member, empty_member.guild, empty_channel, empty_message, bot
    )
