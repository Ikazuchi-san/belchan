from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Any, List, Optional, Tuple, Union

import textwrap

import tabulate
from discord.ext import commands
from pydantic import Field

from ..i18n import _, lazy_gettext

from .base import Model


class DocumentationBase(Model):
    pass


class ArgumentDocumentation(DocumentationBase):
    """Documentation for a command argument.

    Attributes:
        name: The name of the argument.
        doc: The documentation for the argument.
        default: The default value of the argument.
        optional: Flags the argument as optional.
        variadic: Flags the argument as variadic.
    """

    name: str
    doc: lazy_gettext
    default: Optional[Any] = None
    optional: Optional[bool] = False
    variadic: Optional[bool] = False

    def make_argument_doc(self) -> str:
        """Returns the argument in the format required for the usage lines."""
        name: str = self.name

        if self.variadic:
            name = self.name + "..."

        if self.optional:
            if self.default is not None and len(str(self.default)) != 0:
                return f"[{name}={self.default}]"

            return f"[{name}]"
        else:
            return f"<{name}>"

    def make_short_help_row(self, locale: str) -> Tuple[str, str]:
        return (self.name, self.doc.localize(locale))

    def localize(self, locale: str) -> ArgumentDocumentation:
        """Return a localized version of the argument documentation."""
        return ArgumentDocumentation(
            name=self.name,
            doc=self.doc.localize(locale),
            default=self.default,
            optional=self.optional,
            variadic=self.variadic,
        )


class CommandExample(DocumentationBase):
    command: str
    result: Optional[Union[str, lazy_gettext]] = None


class CommandDocumentation(DocumentationBase):
    command: str
    doc: lazy_gettext
    aliases: Optional[List[str]] = Field(default_factory=list)
    args: Optional[List[ArgumentDocumentation]] = Field(default_factory=list)
    examples: Optional[List[CommandExample]] = Field(default_factory=list)

    def make_help(
        self,
        ctx: commands.Context,
        indent: Optional[int] = 0,
        parents: Optional[List[str]] = None,
    ) -> commands.Paginator:
        ret: commands.Paginator = commands.Paginator(prefix="", suffix="")

        formatted_args: List[str] = []

        # Generate argument list
        for arg in self.args:
            if arg.optional:
                formatted_args.append(arg.make_argument_doc())
            else:
                formatted_args.append(arg.make_argument_doc())

        # Generate command invokation line
        if len(self.aliases) == 0:
            if parents is not None:
                ret.add_line(
                    "```css\n"
                    + f"{ctx.prefix}"
                    + " ".join(parents)
                    + f" {self.command} "
                    + " ".join(formatted_args)
                    + "\n"
                    + "```"
                )
            else:
                ret.add_line(
                    "```css\n"
                    + f"{ctx.prefix}{self.command} "
                    + " ".join(formatted_args)
                    + "\n"
                    + "```"
                )
        else:
            if parents is not None:
                ret.add_line(
                    "```css\n"
                    + f"{ctx.prefix}"
                    + " ".join("parents")
                    + f" [{self.command}|"
                    + "|".join(self.aliases)
                    + "] "
                    + " ".join(formatted_args)
                    + "\n"
                    + "```"
                )
            else:
                ret.add_line(
                    "```css\n"
                    + f"{ctx.prefix}[{self.command}|"
                    + "|".join(self.aliases)
                    + "] "
                    + " ".join(formatted_args)
                    + "\n"
                    + "```"
                )

        for line in self.doc.localize(ctx.locale).splitlines():
            ret.add_line(line)

        # Generate arguments documentation, if needed
        if len(self.args) > 0:
            ret.add_line("")
            ret.add_line(_("**Arguments**:", ctx.locale))

            args: commands.Paginator = commands.Paginator(prefix="```apache")

            table_rows: List[Tuple[str, str]] = []

            for arg in self.args:
                table_rows.append(arg.make_short_help_row(ctx.locale))

            for line in tabulate.tabulate(
                table_rows, tablefmt="plain"
            ).splitlines():
                args.add_line(line)

            for page in args.pages:
                ret.add_line(str(page))

        if len(self.examples) > 0:
            ret.add_line("")
            ret.add_line(_("**Examples**:", ctx.locale))

            for example in self.examples:
                if parents is not None:
                    ret.add_line(
                        "\n"
                        + _(":hash: `{command}`", ctx.locale).format(
                            command=(
                                f"{ctx.prefix}"
                                + " ".join(parents)
                                + f" {example.command}"
                            )
                        )
                        + "\n"
                        + _(":arrow_right: {result}", ctx.locale).format(
                            result=example.result
                        )
                    )
                else:
                    ret.add_line(
                        "\n"
                        + _(":hash: `{command}`", ctx.locale).format(
                            command=f"{ctx.prefix}{example.command}"
                        )
                        + "\n"
                        + _(":arrow_right: {result}", ctx.locale).format(
                            result=example.result
                        )
                    )

        return ret

    def make_short_help_row(self, locale: str) -> Tuple[str, str]:
        return (self.command, self.doc.localize(locale).splitlines()[-1])

    def localize(self, locale: str) -> CommandDocumentation:
        """Return a localized version of the command documentation."""
        return CommandDocumentation(
            command=self.command,
            doc=self.doc.localize(locale),
            args=[a.localize(locale) for a in self.args],
            examples=self.examples,
        )


class GroupDocumentation(CommandDocumentation):
    subcommand_required: Optional[bool] = False
    default_subcommand: Optional[str] = None

    def make_help(
        self,
        ctx: commands.Context,
        indent: Optional[int] = 0,
        subcommands: Optional[bool] = False,
    ) -> commands.Paginator:
        ret: commands.Paginator = commands.Paginator(prefix="", suffix="")

        formatted_args: List[str] = []

        if subcommands:
            if self.subcommand_required:
                formatted_args.append("<subcommand>")
            elif self.default_subcommand:
                formatted_args.append(f"[subcommand={self.default_subcommand}]")
            else:
                formatted_args.append(f"[subcommand]")

        # Generate argument list
        for arg in self.args:
            if arg.optional:
                formatted_args.append(arg.make_argument_doc())
            else:
                formatted_args.append(arg.make_argument_doc())

        # Generate command invokation line
        if len(self.aliases) == 0:
            ret.add_line(
                "```css\n"
                + f"{ctx.prefix}{self.command} "
                + " ".join(formatted_args)
                + "\n"
                + "```"
            )
        else:
            ret.add_line(
                "```css\n"
                + f"\n{ctx.prefix}[{self.command}|"
                + "|".join(self.aliases)
                + "] "
                + " ".join(formatted_args)
                + "\n"
                + "```"
            )

        for line in self.doc.localize(ctx.locale).splitlines():
            ret.add_line(line)

        # Generate arguments documentation, if needed
        if len(self.args) > 0:
            ret.add_line("")
            ret.add_line(_("**Arguments**:", ctx.locale))

            args: commands.Paginator = commands.Paginator(prefix="```apache")

            table_rows: List[Tuple[str, str]] = []

            for arg in self.args:
                table_rows.append(arg.make_short_help_row(ctx.locale))

            for line in tabulate.tabulate(
                table_rows, tablefmt="plain"
            ).splitlines():
                args.add_line(line)

            for page in args.pages:
                ret.add_line(str(page))

        if len(self.examples) > 0:
            ret.add_line("")
            ret.add_line(_("**Examples**:", ctx.locale))

            for example in self.examples:
                ret.add_line(
                    "\n"
                    + _(":hash: `{command}`", ctx.locale).format(
                        command=f"{ctx.prefix}{example.command}"
                    )
                    + "\n"
                    + _(":arrow_right: {result}", ctx.locale).format(
                        result=example.result
                    )
                )

        return ret


class ModuleDocumentation(DocumentationBase):
    name: str
    doc: lazy_gettext
    commands: Optional[List[CommandDocumentation]] = Field(default_factory=list)

    def get_command(self, command: str) -> CommandDocumentation:
        """Returns the command documentation object for the given command.

        Args:
            command: The command to search for.

        Raises:
            commands.CommandNotFound: When the command does not exist in the
                current module.
        """
        for c in self.commands:
            if c.command == command:
                return c
            elif command in c.aliases:
                return c

        raise commands.CommandNotFound(message=command)

    async def make_help(
        self, ctx: commands.Context
    ) -> Tuple[commands.Paginator, commands.Paginator]:
        """Returns the help for the module.

        - Module documentation is shown
        - Commands only shown as <command name> <short help>
        """
        hdr: commands.Paginator = commands.Paginator(prefix="", suffix="")

        hdr.add_line(f"**{self.name}**:")
        hdr.add_line(self.doc.localize(ctx.locale), empty=True)
        hdr.add_line(_("**Commands**:", locale=ctx.locale))

        table_rows: List[Tuple[str, str]] = []

        cmds: commands.Paginator = commands.Paginator(prefix="```apache")

        for command in self.commands:
            # Only add commands the user is actually allowed to run in the
            # context to the list.
            try:
                if await ctx.bot.get_command(command.command).can_run(ctx):
                    table_rows.append(command.make_short_help_row(ctx.locale))
            except commands.CheckFailure:
                pass

        for line in tabulate.tabulate(
            table_rows, tablefmt="plain"
        ).splitlines():
            cmds.add_line(line)

        return (hdr, cmds)

    async def make_short_help(self, ctx: commands.Context) -> str:
        """Returns the short help for the module.

        - Module documentation isn't shown
        - Commands only shown as <command name> <short help>
        """
        if len(self.commands) == 0:
            return ""

        ret: str = f"{self.name}:\n"

        table_rows: List[Tuple[str, str]] = []

        for command in self.commands:
            # Only add commands the user is actually allowed to run in the
            # context to the list.
            try:
                if await ctx.bot.get_command(command.command).can_run(ctx):
                    table_rows.append(command.make_short_help_row(ctx.locale))
                else:
                    pass
            except commands.CheckFailure:
                pass

        if len(table_rows) == 0:
            return ""

        ret += textwrap.indent(
            tabulate.tabulate(table_rows, tablefmt="plain"), "    "
        )

        return ret

    def localize(self, locale: str) -> ModuleDocumentation:
        """Return a localized version of the module documentation."""
        return ModuleDocumentation(
            name=self.name,
            doc=self.doc.localize(locale),
            commands=[c.localize(locale) for c in self.commands],
        )
