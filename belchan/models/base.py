from __future__ import annotations
from typing import TYPE_CHECKING, Type, Dict, Any, TypeVar


from pydantic import (
    BaseModel,
    Field,
    validator,
    root_validator,
    constr,
    conint,
    PositiveInt,
    PositiveFloat,
    ValidationError,
)

if TYPE_CHECKING:
    from ..application import Context


T = TypeVar("T")


class Model(BaseModel):
    @classmethod
    def from_dict(cls: Type[T], data: Dict[str, Any]) -> T:
        return cls.parse_obj(data)
