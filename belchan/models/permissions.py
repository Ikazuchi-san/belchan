from typing import Set

from .base import Model

from ..i18n import lazy_gettext


class ModulePermission(Model):
    """A permission declaration object."""

    name: str
    """ The name of the permission.
    """
    description: lazy_gettext
    """ The description string of the permission.
    """


class InvalidPermission(Exception):
    def __init__(self, *args):
        super().__init__(*args)


class MissingBotPermission(Exception):
    def __init__(self, permission):
        super().__init__(permission)


class Role(Model):
    id: int
    name: str
    permissions: Set[str]
