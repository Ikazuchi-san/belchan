from typing import Any

from enum import Enum as Enum_

from ..context import ContextBase

from discord.ext.commands.errors import BadArgument


class Enum(Enum_):
    __doc__ = Enum_.__doc__

    @classmethod
    async def convert(cls, ctx: ContextBase, value: Any):
        return cls(value)
