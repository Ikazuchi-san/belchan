import asyncio
import subprocess
import sys
import textwrap
from pathlib import Path

import click
import git

from . import config
from . import application


async def run_main(config_file, secrets_file, environment):
    config_files = []

    if config_file is not None:
        config_files.append(config_file)

    if secrets_file is not None:
        config_files.append(secrets_file)

    app: application.Application = application.make_application()(
        config.load_config(config_files, environment)
    )

    try:
        await app.start(app.config.discord.bot_token)
    except (KeyboardInterrupt, InterruptedError):
        await app.logout()


@click.group()
def main():
    pass


@main.command()
@click.option("--config-file", "-c", default=None)
@click.option("--secrets-file", "-s", default=None)
@click.option("--environment", "-e", default="production")
def run(config_file, secrets_file, environment):
    loop = asyncio.get_event_loop()

    loop.run_until_complete(run_main(config_file, secrets_file, environment))

    loop.close()


@main.command("init-config")
@click.option(
    "--config-file",
    "-c",
    default="settings.yaml",
    help="The location of the configuration file to use.",
)
def init_config(config_file):
    config.generate_config(config_file)

    print(
        "Configuration file has been created. Edit as required before starting"
        " the bot."
    )


@main.group()
def module():
    pass


@module.command()
@click.argument("module_name")
def install(module_name: str):
    """Install a local module and its dependencies."""
    if not Path(Path(__file__).parent / "modules" / module_name).exists():
        print(
            f'Module "{module}" does not exist. Check that the module folder ('
            + str(Path(__file__).parent / "modules" / module_name)
            + ") exists.",
            file=sys.stderr,
        )

        return

    if not Path(
        Path(__file__).parent / "modules" / module_name / "pyproject.toml"
    ).exists():
        print(
            f'Module "{module}" does not have a "pyproject.toml". Cannot'
            " be installed using this command.",
            file=sys.stderr,
        )

        return

    subprocess.run(
        ["poetry", "install"],
        cwd=str(Path(__file__).parent / "modules" / module_name),
    )


@module.command()
@click.argument("module_name")
@click.argument("url")
def get(module_name: str, url: str):
    """Download a remote module without installing it."""
    try:
        git.Repo.clone_from(
            url, Path(__file__).parent / "modules" / module_name
        )
    except git.exc.GitError as exc:
        print(f"ERROR: '{url}' is not a valid git repository.", file=sys.stderr)
        print(
            textwrap.indent(
                "Make sure the repository exists and can be reached and accessed"
                " without password.",
                "    ",
            ),
            file=sys.stderr,
        )
