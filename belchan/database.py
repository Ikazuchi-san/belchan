from __future__ import annotations
from typing import Sequence, Type, TYPE_CHECKING

import os
import inspect
import functools
from contextvars import ContextVar
from contextlib import (
    ContextDecorator,
    asynccontextmanager,
    AbstractContextManager,
    AbstractAsyncContextManager,
)

import stringcase
from peewee import (
    Database,
    DatabaseProxy,
    DoesNotExist,
    IntegrityError,
    prefetch,
)
from playhouse.pool import (
    PooledMySQLDatabase,
    PooledPostgresqlExtDatabase,
    PooledSqliteDatabase,
)

if TYPE_CHECKING:
    from .models.database import Model, BotConfig

connection: ContextVar = ContextVar("connection")
""" Context-local variable that stores the database connection.

Use :func:db_session or :func:db_context to access.
"""


class _db_context(
    AbstractContextManager, AbstractAsyncContextManager, ContextDecorator
):
    def __call__(self, func=None):
        if inspect.iscoroutinefunction(func):

            @functools.wraps(func)
            async def wrapper(*args, **kwargs):
                with self:
                    return await func(*args, **kwargs)

            return wrapper
        elif func is not None:

            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                with self:
                    return func(*args, **kwargs)

            return wrapper
        else:
            return self

    def __enter__(self) -> Connection:
        self.db: DatabaseProxy = Connection.get_singleton().database_proxy

        self.__token = connection.set(self.db._connect())

        return self.db

    def __exit__(self, *exc):
        self.db._close(connection.get())

        connection.reset(self.__token)

    async def __aenter__(self) -> Connection:
        return self.__enter__()

    async def __aexit__(self, *exc) -> Connection:
        return self.__exit__(*exc)


def make_db_context(func=None) -> _db_context:
    if func:
        return _db_context()(func)
    else:
        return _db_context()


# Keep compatibility to old db_context and db_session
db_session = db_context = make_db_context


class Connection:
    __singleton: Connection = None
    database_proxy: DatabaseProxy = DatabaseProxy()

    def __init__(self):
        Connection.__singleton = self
        Connection.__connection: Database = None

    def connect(self, config: BotConfig):
        if config.database.driver == "sqlite":
            if config.database.filename != ":memory:":
                self.__connection = PooledSqliteDatabase(
                    os.path.join(config.instance_path, config.database.filename)
                )
            else:
                self.__connection = PooledSqliteDatabase(":memory:")
        elif config.database.driver == "postgres":
            self.__connection = PooledPostgresqlExtDatabase(
                config.database.database,
                user=config.database.username,
                password=config.database.password,
                host=config.database.host,
            )
        elif config.database.driver == "mysql":
            self.__connection = PooledMySQLDatabase(
                config.database.database,
                user=config.database.username,
                password=config.database.password,
                host=config.database.host,
            )
        else:
            raise ValueError(
                f"Invalid database driver: {config.database.driver}"
            )

        self.__connection.connect()

        self.database_proxy.initialize(self.__connection)

    def ensure_tables(self, tables: Sequence[Type[Model]]):
        """Creates the tables for the given models if they don't exist."""

        self.__connection.create_tables(tables)

    def disconnect(self):
        pass

    @property
    def database(self) -> Database:
        return self.__connection

    @classmethod
    def get_singleton(cls):
        if cls.__singleton is None:
            cls.__singleton = Connection()

        return cls.__singleton


def make_table_name(model_class: Type[Model]) -> str:
    model_name = model_class.__name__

    # Lowercase first letter so that snakecase works.
    if len(model_name) > 1:
        model_name = model_name[0].lower() + model_name[1:]
    else:
        # Handle single-letter model names.. for whenever someone has that
        # brilliant idea.
        model_name = model_name.lower()

    # Convert the table name to snake_case
    model_name = stringcase.snakecase(model_name)

    # Pluralize table name, if neccessary.
    if model_name[-1] != "s":
        model_name += "s"

    return model_name
