from __future__ import annotations
from typing import Union, Optional, Sequence

import discord
from discord.ext import commands
from discord.ext.commands.errors import MissingPermissions

from .models.database import RolePermissions


async def member_has_permission(
    member: discord.Member, permission: str
) -> bool:
    """Returns true if the given member has permissions for the given command."""

    for role in member.roles:
        perms: RolePermissions = RolePermissions.get_or_none(
            RolePermissions.role_id == role.id
        )

        if perms is not None:
            # If above query returns None, the current role does not have the
            # permission.
            if permission in perms.permissions:
                return True

    return False


def require_permission(
    permission: Union[str, discord.flags.flag_value],
    guild: Optional[bool] = False,
):
    """Checks if the author has all of the specified permissions.

    Args:
        permission: The permission to check for.
        guild: If set, use guild-wide permissions instead of permissions in the
            current channel for the check.
    """

    if isinstance(permission, discord.flags.flag_value):
        if guild:
            permission = discord.Permissions(permission.flag)

            async def predicate(ctx: discord.Context):

                if ctx.author.guild_permissions.is_strict_superset(
                    discord.Permissions(permission.flag)
                ):
                    return True

                raise MissingPermissions(permission)

            return commands.check(predicate)
        else:

            async def predicate(ctx: discord.Context):
                if ctx.author.permissions.is_strict_superset(
                    discord.Permissions(permission.flag)
                ):
                    return True

                raise MissingPermissions(permission)

            return commands.check(predicate)
    else:

        async def predicate(ctx: discord.Context):
            nonlocal permission

            if await member_has_permission(ctx.author, permission):
                return True
            else:
                raise MissingPermissions([permission])

        return commands.check(predicate)


def require_any_permission(
    permissions: Sequence[Union[str, discord.flags.flag_value]],
    guild: Optional[bool] = False,
):
    """Checks if the author's has any of the specified permissions.

    Args:
        permission: A list of permissions to check.
        guild: If set, use guild-wide permissions instead of permissions in the
            current channel for the check.
    """

    if guild:

        async def predicate(ctx: discord.Context):
            for perm in permissions:
                if isinstance(perm, discord.flags.flag_value):
                    if ctx.author.guild_permissions.is_strict_superset(
                        discord.Permissions(perm.flag)
                    ):
                        return True
                else:
                    if await member_has_permission(ctx.author, perm):
                        return True

            raise MissingPermissions([str(p) for p in permissions])

        return commands.check(predicate)
    else:

        async def predicate(ctx: discord.Context):
            for perm in permissions:
                if isinstance(perm, discord.flags.flag_value):
                    if ctx.author.guild_permissions.is_strict_superset(
                        discord.Permissions(perm.flag)
                    ):
                        return True
                else:
                    if await member_has_permission(ctx.author, perm):
                        return True

            raise MissingPermissions([str(p) for p in permissions])

        return commands.check(predicate)


def require_enabled(module: str):
    """Checks if the requested feature is enabled."""

    async def predicate(ctx: discord.Context):
        if isinstance(ctx.channel, discord.DMChannel) or isinstance(
            ctx.channel, discord.GroupChannel
        ):
            # Permissions are guild based.
            return False

        config = ctx.bot.get_module_config(ctx.guild.id, module.lower())

        return config.enabled

    return commands.check(predicate)
