"""Command related classes and functions.
"""

from typing import Callable, Tuple, Optional, Set, List, Union

import functools
import textwrap

import discord
from pydantic import validate_arguments
from discord.ext import commands
from discord.ext.commands import (
    guild_only,
    is_owner,
    Cog,
    has_permissions,
    has_role,
    has_any_role,
    has_guild_permissions,
    Paginator,
    Converter,
    Greedy,
    errors,
)
from tabulate import tabulate

from .i18n import _
from .models.docs import CommandDocumentation, GroupDocumentation
from .models.discord import Event
from .context import ContextBase
from .permissions import require_enabled, require_permission


class Command(commands.Command):
    def __init__(
        self,
        func: Callable,
        doc: CommandDocumentation,
        required_permissions: Optional[Set[discord.flags.flag_value]] = None,
        *args,
        **kwargs,
    ):
        self.__documentation__: CommandDocumentation = doc

        super().__init__(func, *args, **kwargs)

        # if required_permissions is not None:
        #     for permission in require_permission:
        #         self.add_check(require_permission(permission))

    async def make_help(
        self,
        ctx: ContextBase,
        indent: Optional[int] = None,
        parents: Optional[List[str]] = None,
    ) -> Paginator:
        """Returns the long help for the command.

        Args:
            ctx:
            indent:
            parents:
        """
        return self.__documentation__.make_help(ctx, indent, parents)

    async def make_short_help_row(self, ctx: ContextBase) -> Tuple[str, str]:
        return self.__documentation__.make_short_help_row(ctx.locale)


class Group(commands.Group):
    def __init__(
        self,
        *args,
        doc: GroupDocumentation,
        **kwargs,
    ):
        self.__documentation__: GroupDocumentation = doc

        super().__init__(*args, **kwargs)

    @validate_arguments(config=dict(arbitrary_types_allowed=True))
    def command(
        self,
        doc: CommandDocumentation,
        required_permissions: Optional[
            Set[Union[str, discord.flags.flag_value]]
        ] = None,
        hidden: Optional[bool] = False,
        *args,
        **kwargs,
    ):
        def decorator(func):
            kwargs.setdefault("parent", self)
            result = command(
                doc, required_permissions, hidden, *args, **kwargs
            )(func)
            self.add_command(result)
            return result

        return decorator

    @validate_arguments(config=dict(arbitrary_types_allowed=True))
    def group(
        self,
        doc: GroupDocumentation,
        required_permissions: Optional[
            Set[Union[str, discord.flags.flag_value]]
        ] = None,
        *args,
        **kwargs,
    ):
        def decorator(func):
            kwargs.setdefault("parent", self)
            kwargs.setdefault("cls", Group)
            result = group(doc, required_permissions, *args, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator

    async def make_help(
        self, ctx: ContextBase, indent: Optional[int] = None
    ) -> str:
        """Returns the long help for the group."""
        pg: Paginator = self.__documentation__.make_help(
            ctx, indent, len(self.commands) > 0
        )

        if len(self.commands) > 0:
            subcommands = await self.make_subcommands_help(ctx)

            if len(subcommands) > 0:
                pg.add_line("")
                pg.add_line(_("**Subcommands:**", ctx.locale))
                pg.add_line(f"```apache\n{subcommands}\n```")

        return pg

    async def make_subcommands_help(self, ctx: ContextBase) -> str:
        """Returns the short help for subcommands.

        - Commands only shown as <command name> <short help>
        """
        if len(self.commands) == 0:
            return ""

        ret: str = ""

        table_rows: List[Tuple[str, str]] = []

        for command in self.commands:
            # Do not show hidden commands.
            if command.hidden:
                continue

            # Only add commands the user is actually allowed to run in the
            # context to the list.
            try:
                if await command.can_run(ctx):
                    table_rows.append(await command.make_short_help_row(ctx))
                else:
                    pass
            except commands.CheckFailure:
                pass

        if len(table_rows) == 0:
            return ""

        ret += tabulate(table_rows, tablefmt="plain")

        return ret

    async def make_short_help_row(self, ctx: ContextBase) -> Tuple[str, str]:
        return self.__documentation__.make_short_help_row(ctx.locale)


@validate_arguments(config=dict(arbitrary_types_allowed=True))
def command(
    doc: CommandDocumentation,
    required_permissions: Optional[
        Set[Union[str, discord.flags.flag_value]]
    ] = None,
    hidden: Optional[bool] = False,
    *args,
    **kwargs,
):
    """Decorator for creating a command from an async function.

    Args:
        doc: The documentation for the command.
        required_permissions: A list of required permissions for running the
            command.
        hidden: If true, the command will not be shown in the help command.
    """
    if not isinstance(doc, CommandDocumentation):
        raise "Invalid type for doc: {type(doc)}"

    return commands.command(
        cls=Command,
        doc=doc,
        required_permissions=required_permissions,
        hidden=hidden,
        *args,
        **kwargs,
    )


@validate_arguments(config=dict(arbitrary_types_allowed=True))
def group(
    doc: GroupDocumentation,
    required_permissions: Optional[
        Set[Union[str, discord.flags.flag_value]]
    ] = None,
    hidden: Optional[bool] = False,
    *args,
    **kwargs,
) -> Group:
    """Decorator for creating a command from an async function.

    Args:
        doc: The documentation for the group.
        required_permissions: A list of required permissions for running
            commands in the group.
        hidden: If true, the group will not be shown in the help command.
    """
    if not isinstance(doc, GroupDocumentation):
        raise "Invalid type for doc: {type(doc)}"

    kwargs.pop("doc", None)

    return commands.group(
        *args,
        doc=doc,
        required_permissions=required_permissions,
        hidden=hidden,
        cls=Group,
        **kwargs,
    )


def listener(event: Optional[Event] = None):
    """Decorator for creating a listener.

    Args:
        event: The event to listen to. If not provided, the name of the function
            will be used to determine the event to listen to.
    """
    if event is not None:
        return commands.Cog.listener(event.value)
    else:
        return commands.Cog.listener()
