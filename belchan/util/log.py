from typing import (
    Any,
    Callable,
    Dict,
    NoReturn,
    Optional,
    Union,
    TypeVar,
    List,
)
from typing_extensions import Protocol

import datetime
import functools
import inspect
from collections import OrderedDict
from dataclasses import dataclass
from asyncio import get_event_loop

import discord

from ..context import ContextBase
from ..database import db_context
from ..models.database import CommandLog
from ..modules import Module
from ..i18n import lazy_gettext

R = TypeVar("R")
T = TypeVar("T")


class LoggerFunction(Protocol):
    async def __call__(
        self, ctx: ContextBase, **kwargs: Dict[str, Any]
    ) -> NoReturn:
        pass


@dataclass
class FieldInfo:
    title: lazy_gettext
    arg: str
    inline: Optional[bool] = False


async def no_channel_logger(ctx: ContextBase, **kwargs: Dict[str, Any]):
    pass


def _make_resolver(
    target: Union[Callable[[ContextBase], R], T]
) -> Callable[[ContextBase], Union[T, R]]:
    if inspect.iscoroutinefunction(target):

        def resolver(ctx) -> Union[T, R]:
            return get_event_loop().run_until_complete(target(ctx))

        return resolver
    elif callable(target):
        return lambda ctx: target(ctx)
    else:
        return lambda ctx: target


async def make_embed_logger(
    fields: List[FieldInfo],
    channel: Union[
        discord.TextChannel,
        discord.DMChannel,
        Callable[[ContextBase], Union[discord.TextChannel, discord.DMChannel]],
    ],
    title: Optional[
        Union[lazy_gettext, Callable[[ContextBase], str], str]
    ] = None,
    body: Optional[
        Union[lazy_gettext, Callable[[ContextBase], str], str]
    ] = None,
    color: Optional[
        Union[
            discord.Color,
            int,
            str,
            Callable[[ContextBase], Union[discord.Color, int, str]],
        ]
    ] = None,
) -> LoggerFunction:
    field_list: Dict[FieldInfo, Callable[[Dict[str, Any]], Any]] = []

    for field in fields.items():
        # Add the field with the given name as a lambda. This allows the field
        # value to be resolved whenever the function is called.
        field_list[field] = lambda kwargs: kwargs[field.arg]

    title_resolve: Callable[
        [ContextBase], Union[lazy_gettext, str]
    ] = _make_resolver(title)
    body_resolve: Callable[
        [ContextBase], Union[lazy_gettext, str]
    ] = _make_resolver(body)
    channel_resolve: Callable[
        [ContextBase], Union[discord.TextChannel, discord.DMChannel]
    ] = _make_resolver(channel)
    color_resolve: Callable[
        [ContextBase], Union[discord.TextChannel, discord.DMChannel]
    ] = _make_resolver(color)

    async def generated_logger(ctx: ContextBase, **kwargs: Dict[str, Any]):
        nonlocal body_resolve
        nonlocal channel_resolve

        title = title_resolve(ctx)

        if isinstance(title, lazy_gettext):
            title = title.localize(ctx.locale)

        body = body_resolve(ctx)

        if isinstance(body, lazy_gettext):
            body = body.localize(ctx.locale)

        color = color_resolve(ctx)
        channel = channel_resolve(ctx)

        if channel is None:
            return

        embed: discord.Embed = discord.Embed(
            color=color, title=title, description=body
        )

        for field, resolver in field_list.items():
            embed.add_field(
                name=field.title.localize(ctx.locale),
                value=resolver(ctx, kwargs),
                inline=field.inline,
            )

        await channel.send(embed=embed)

    return generated_logger


def logged_command(
    extra_args: Optional[
        Callable[[ContextBase, Dict[str, Any]], Dict[str, Any]]
    ] = lambda ctx, args: {},
    channel_logger: Optional[LoggerFunction] = no_channel_logger,
):
    """Wrapper that logs any successful execution of a command.

    Args:
        extra_args: Function that can modify the logged arguments.
            The returned dict is used to update kwargs.

        `ctx` and `kwargs` is passed to it.
        channel_logger: An async function that sends a log message.

        `ctx` and `kwargs` is passed to it.
    """

    def make_logger(func) -> LoggerFunction:
        arg_names = [
            n for n in func.__annotations__.keys() if n not in ["self", "ctx"]
        ]

        @functools.wraps(func)
        async def logged_method(
            module: Module,
            ctx: ContextBase,
            *args,
            **kwargs,
        ) -> Any:
            """Wrapper that logs any successful execution of a command.

            Args:
                ctx: The command context.
                _logger_extra_args_: Function that can modify the logged
                    arguments.
                    The returned dict is used to update kwargs.

                    `ctx` and `kwargs` is passed to it.
                kwargs: The Args passed along to the command and
                    _logger_extra_args_.
            """
            ordered_args: Dict[str, Any] = OrderedDict()
            i: int = 0

            for i in range(0, len(args)):
                ordered_args[arg_names[i]] = args[i]

            for k, v in kwargs.items():
                ordered_args[k] = v

            ret = await func(module, ctx, **ordered_args)

            args = kwargs
            args.update(extra_args(ctx, ordered_args))

            # Convert discord objects to their IDs to prevent json-serialization
            # error.
            for k, v in args.items():
                if isinstance(v, discord.abc.Snowflake):
                    args[k] = v.id

            async with db_context():
                # Create the database entry.
                CommandLog.create(
                    server_id=ctx.guild.id,
                    command=ctx.command.name,
                    user=ctx.author.id,
                    args=args,
                    timestamp=datetime.datetime.now(),
                )

            # Run the logger function.
            await channel_logger(ctx, **args)

            return ret

        return logged_method

    return make_logger
