from __future__ import annotations
from typing import Tuple

from io import BytesIO

from PIL import Image


def from_bytes(data: bytes) -> Image.Image:
    image: Image.Image = Image.open(BytesIO(data), "r")
    image.load()
    image.verify()

    return image
