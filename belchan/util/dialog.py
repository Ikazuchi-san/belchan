from typing import Any, NoReturn, Union, List, Optional, Dict

from datetime import timedelta
from abc import ABC, abstractmethod
from asyncio import Future, TimeoutError, get_event_loop
from dataclasses import dataclass

import discord


AnyChannel = Union[discord.TextChannel, discord.GroupChannel, discord.DMChannel]
AnyUser = Union[discord.Member, discord.User]
AnyEmoji = Union[discord.Emoji, discord.PartialEmoji, str]


@dataclass
class DialogChoice:
    emoji: AnyEmoji
    value: Any
    description: Optional[str] = None


class Dialog(ABC):
    def __init__(self, client: discord.Client):
        self.__future: Future = Future()
        self._client: discord.Client = client
        self._message: discord.Message = None

    @property
    def result(self) -> Future:
        return self.__future

    @result.setter
    def result(self, value: Any) -> NoReturn:
        self.__future.set_result(value)

    @abstractmethod
    async def display(
        self,
        channel: AnyChannel,
        timeout: Optional[timedelta] = timedelta(seconds=20),
        *args,
        **kwargs,
    ) -> discord.Message:
        pass

    async def _make_dialog(
        self,
        channel: AnyChannel,
        embed: discord.Embed,
        choices: List[AnyEmoji],
    ) -> discord.Message:
        """Post the given embed to the given channel.

        Also sets the `_message` attribute."""
        self._message = await channel.send(embed=embed)

        for reaction in choices:
            await self._message.add_reaction(reaction)

        return self._message

    async def cleanup(self, remove: Optional[bool] = False) -> NoReturn:
        """Disable the dialog.

        Dialog is disabled by removing the reactions from the message.

        If `remove` is set to `True`, the message will be deleted entirely
        instead.

        Args:
            remove: If true, remove the message entirely."""
        if remove:
            self._message.delete()
        else:
            await self._message.clear_reactions()


class ChoiceDialogBase(Dialog, ABC):
    def __init__(
        self,
        client: discord.Client,
        title: Optional[str] = None,
        choices: Optional[Union[List[DialogChoice], Dict[AnyEmoji, Any]]] = {},
        text: Optional[str] = None,
        user: Optional[AnyUser] = None,
    ):
        super().__init__(client)

        self.title = title
        self.text = text
        self.user = user
        # self.__choices: Dict[str, DialogChoice] = {str(k): v for k, v in choices.items()}

        if isinstance(choices, dict):
            self.__choices: Dict[str, DialogChoice] = {
                k: DialogChoice(emoji=discord.PartialEmoji(name=k), value=v)
                for k, v in choices.items()
            }
        elif isinstance(choices, list):
            self.__choices: Dict[str, DialogChoice] = {
                str(e.emoji): e for e in choices
            }
        else:
            self.__choices: Dict[str, DialogChoice] = dict()

    def add_choice(
        self, emoji: AnyEmoji, value: Any, description: Optional[str] = None
    ):
        self.__choices[str(emoji)] = DialogChoice(
            emoji=emoji, value=value, description=description
        )

    @property
    def choices(self):
        return self.__choices

    async def display(
        self,
        channel: AnyChannel,
        timeout: Optional[timedelta] = timedelta(seconds=20),
        show_descriptions: Optional[bool] = True,
    ) -> discord.Message:
        """Display the dialog in the given channel until timeout is reached.

        Args:
            channel: The channel to show the dialog in.
            timeout: How long to wait for an answer. When the timeout is
                reached, reactions will be removed.

        Returns:
            The newly created message containing the dialog.
        """
        embed: discord.Embed = discord.Embed(title=self.title)

        if self.user:
            embed.set_author(name=str(self.user), icon_url=self.user.avatar_url)

        if self.text:
            embed.description = self.text

        if show_descriptions:
            # Create the choice description list.
            for choice in self.__choices.values():
                embed.add_field(name=choice.emoji, value=choice.description)

        message: discord.Message = await self._make_dialog(
            channel=channel, embed=embed, choices=self.choices
        )

        loop = get_event_loop()
        loop.create_task(self._response_fetcher(message, timeout, self.user))

        return message

    async def _response_fetcher(
        self,
        message: discord.Message,
        timeout: timedelta,
        user: Optional[AnyUser] = None,
    ) -> NoReturn:
        """The internal task that sets the `result` attribute.

        Args:
            message: The message to watch for reactions.
            timeout: How long to watch the message for reactions.
            user: If given, the user to restrict to. All reactions by other
                users will be ignored."""
        try:
            if user:
                reaction, _ = await self._client.wait_for(
                    "reaction_add",
                    check=lambda r, u: r.message.id == message.id
                    and u.id == user.id
                    and r.emoji in self.__choices,
                    timeout=int(timeout.total_seconds()),
                )
            else:
                reaction, _ = await self._client.wait_for(
                    "reaction_add",
                    check=lambda r, u: r.message.id == message.id
                    and r.emoji in self.choices,
                    timeout=int(timeout.total_seconds()),
                )

            self.result = self.__choices[str(reaction)].value
        except TimeoutError:
            self.result = None
        finally:
            await self.cleanup()


class ChoiceDialog(ChoiceDialogBase):
    def __init__(
        self,
        client: discord.Client,
        title: Optional[str] = None,
        text: Optional[str] = None,
        user: Optional[AnyUser] = None,
        choices: List[DialogChoice] = [],
    ):
        super().__init__(
            client,
            title=title,
            choices={c.emoji: c.value for c in choices},
            text=text,
            user=user,
        )


async def choice(
    client: discord.Client,
    channel: AnyChannel,
    choices: Union[Dict[AnyEmoji, Any], List[DialogChoice]],
    title: Optional[str] = None,
    text: Optional[str] = None,
    user: Optional[AnyUser] = None,
    timeout: Optional[timedelta] = timedelta(seconds=20),
    show_descriptions: Optional[bool] = False,
):
    """Make a ChoiceDialog and await the answer in a single call.

    Args:
        client: The discord client (Bot) object used to display the command.
        channel: The channel the confirmation dialog should be shown in
        title: The title of the confirmation dialog.
        choices: The available choices.
        text: The text in the confirmation dialog.
        user: If provided, only reactions by this user will be recognized.
        timeout: How long this function will wait for an answer.

    Examples:
        >>> await choice(ctx.bot, channel, title="Which Emote?",
        ...     user=ctx.author,
        ...     choices={":smile:": True, ":frowning:": False})
        Displays choice dialog with the title "Which Emote?" for 20 seconds.
        Only recognizes reactions by the user in `ctx.author`.
    """
    choice: ChoiceDialog = None

    if isinstance(choices, list):
        choice = ChoiceDialog(client, title, text, user)

        for c in choices:
            choice.add_choice(c.emoji, c.value, c.description)
    else:
        choice = ChoiceDialog(client, title, text, user)

        for emoji, value in choices.items():
            choice.add_choice(emoji, value, description=None)

    await choice.display(channel, timeout, show_descriptions)

    return await choice.result


class ConfirmationDialog(ChoiceDialogBase):
    """An alias for a ChoiceDialog with ✅ and ❌ as choices.

    Examples:
        >>> confirmation = ConfirmationDialog(client, "Really?", user)
        ... await confirmation.display(channel)
        ... await confirmation.result"""

    def __init__(
        self,
        client: discord.Client,
        title: Optional[str] = None,
        text: Optional[str] = None,
        user: Optional[AnyUser] = None,
    ):
        super().__init__(
            client,
            title=title,
            text=text,
            choices={"✅": True, "❌": False},
            user=user,
        )


async def confirmation(
    client: discord.Client,
    channel: AnyChannel,
    title: Optional[str] = None,
    text: Optional[str] = None,
    user: Optional[AnyUser] = None,
    timeout: Optional[timedelta] = timedelta(seconds=20),
) -> Optional[bool]:
    """Make a ConfirmationDialog and await the answer in a single call.

    Args:
        client: The discord client (Bot) object used to display the command.
        channel: The channel the confirmation dialog should be shown in
        title: The title of the confirmation dialog.
        text: The text in the confirmation dialog.
        user: If provided, only reactions by this user will be recognized.
        timeout: How long this function will wait for an answer.

    Examples:
        >>> await confirmation(ctx.bot, channel=ctx.channel, title="Really?",
        ...     user=ctx.author)
        Displays confirmation dialog with the title "Really" for 20 seconds.
        Only recognizes reactions by the user in `ctx.author`."""
    confirmation: ConfirmationDialog = ConfirmationDialog(
        client=client, title=title, text=text, user=user
    )

    await confirmation.display(
        channel=channel, timeout=timeout, show_descriptions=False
    )

    return await confirmation.result


class Poll(Dialog):
    def __init__(
        self,
        client: discord.Client,
        title: Optional[str] = None,
        text: Optional[str] = None,
        image: Optional[str] = None,
        choices: List[DialogChoice] = [],
    ):
        super().__init__(client)

        self.title = title
        self.text = text
        self.__choices: List[DialogChoice] = choices

    async def display(
        self,
        channel: AnyChannel,
        timeout: Optional[timedelta] = None,
        show_descriptions: Optional[bool] = True,
    ) -> discord.Message:
        embed: discord.Embed = discord.Embed(title=self.title)

        if self.text:
            embed.description = self.text

        if show_descriptions:
            # Create the choice description list.
            for choice in self.choices:
                embed.add_field(name=choice.emoji, value=choice.description)

        message: discord.Message = await self._make_dialog(
            channel=channel, embed=embed, choices=self.__choices
        )

        return message

    async def count(self):
        pass
