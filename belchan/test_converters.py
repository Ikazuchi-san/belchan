import dateutil.parser

from . import converters


def test_date_conversion():
    print(
        dateutil.parser.parse(
            "monday", parserinfo=converters.get_locale_parser("en")
        )
    )
