"""Filesystem related classes and functions.
"""

from __future__ import annotations
from typing import Optional, Union, List, Sequence

import io
import re
import pathlib
import random
from pathlib import Path
from os import PathLike
from enum import Enum

import discord
import aiofiles


class FileTypeMatcher:
    """Matches file types to categories.

    Attributes:
        extensions (List[str]): The file extensions to match.
        fnpattern (str): Posix file matching pattern.
    """

    def __init__(self, extensions: Sequence[str]):
        self.extensions: Sequence[str] = extensions

        if "*" in extensions:
            self.fnpattern: re.Pattern = re.compile(".*")
        else:
            self.fnpattern: re.Pattern = re.compile(
                ".*(" + "|".join([f"\{e}" for e in extensions]) + ")"
            )

    def file_matches(self, filename: PathLike) -> bool:
        """Returns true, if the given file matches the file type.

        Args:
            filename: The filename to check.
        """
        ret = bool(self.fnpattern.match(filename))

        return ret

    def list_file_matches(
        self, path: pathlib.Path, recursive: Optional[bool] = False
    ) -> List[pathlib.Path]:
        """Lists all files of the matcher's type in the given path.

        Args:
            path: The path to list the files from.
            recursive: If true, recursively list all matching files from all
                subfolders in the given path.
        """
        if recursive:
            ret: List[pathlib.Path] = []

            # If any of the file entries is "*", just list all files.
            if "*" in self.extensions:
                return [e for e in path.rglob("*") if not e.is_dir()]

            for ext in self.extensions:
                ret.extend([e for e in path.rglob(f"*{ext}") if not e.is_dir()])

            return ret
        else:
            ret: List[pathlib.Path] = []

            # If any of the file entries is "*", just list all files.
            if "*" in self.extensions:
                return [e for e in path.rglob("*") if not e.is_dir()]

            for ext in self.extensions:
                ret.extend([e for e in path.rglob(f"*{ext}") if not e.is_dir()])

            return ret


class FileType(FileTypeMatcher, Enum):
    """A list of supported file type categories."""

    any: FileType = ["*"]
    """ Any kind of file.
    """
    image: FileType = [
        ".apng",
        ".gif",
        ".jpeg",
        ".jpg",
        ".png",
        ".webp",
        ".svg",
    ]
    """Image files.
    """
    archive: FileType = [
        ".zip",
        ".tar",
        ".tar.gz",
        ".tar.xz",
        ".7z",
        ".rar",
    ]
    """Archive files.
    """
    executable: FileType = [".app", ".exe"]
    """Executables.
    """
    text: FileType = [".txt"]
    """Text files
    """
    python: FileType = [".py", ".pyc", ".pyd", ".pyo"]


class File:
    def __init__(self, path: Optional[Path] = None):
        self.path = path


class Bundle:
    def __init__(
        self, name: str, path: PathLike, per_server: Optional[bool] = False
    ):
        self.name = name
        self.path = pathlib.Path(path)
        self.per_server = per_server

    def ensure(self):
        """Ensures the bundle's filesystem path exists."""
        self.path.mkdir(parents=True, exist_ok=True)

    def put(
        self,
        filename: PathLike,
        file: Union[discord.File, io.BytesIO, io.StringIO],
        server: Optional[discord.Guild] = None,
    ):
        if self.per_server:
            path: Path = pathlib.Path(self.path, server, filename)

            if not path.exists():
                path.mkdir(parents=True, exists_ok=True)
        else:
            path: Path = pathlib.Path(self.path, filename)

        if isinstance(file, io.IOBase):
            with open(path, "wb") as fp:
                fp.write(file.read())
        else:
            with open(path, "wb") as fp:
                fp.write(file.fp.read())

    async def aput(
        self,
        filename: PathLike,
        file: Union[discord.File, io.BytesIO, io.StringIO],
        server: Optional[int] = None,
    ):
        if self.per_server:
            path: Path = pathlib.Path(self.path, server, filename)

            if not path.exists():
                path.mkdir(parents=True, exists_ok=True)
        else:
            path: Path = pathlib.Path(self.path, filename)

        if isinstance(file, io.IOBase):
            async with aiofiles.open(path, "wb") as fp:
                await fp.write(file.read())
        else:
            async with aiofiles.open(path, "wb") as fp:
                await fp.write(file.fp.read())

    def get(
        self, filename: PathLike, server: Optional[int] = None
    ) -> discord.File:
        """Returns the requested file, if it exists.

        Returns:
            A :class:`discord.Files` with the requested file.
        """

        if self.per_server:
            path: Path = pathlib.Path(self.path, server, filename)
        else:
            path: Path = pathlib.Path(self.path, filename)

        with open(path, "rb") as fp:
            return discord.File(fp.read())

    async def aget(
        self, filename: PathLike, server: Optional[int] = None
    ) -> discord.File:
        if self.per_server:
            path: Path = pathlib.Path(self.path, server, filename)
        else:
            path: Path = pathlib.Path(self.path, filename)

        async with aiofiles.open(path, "rb") as fp:
            return discord.File(await fp.read(), pathlib.Path(filename).name)

    def create_bundle(self, name: str, path: PathLike) -> Bundle:
        """Create a bundle at the specified path.

        Args:
            name: The name of the bundle. (for later use)
            path: The path of the bundle relative to its parent.
        """
        return Bundle(name, pathlib.Path(self.path, path))

    def get_bundle(self, path: PathLike):
        return Bundle(pathlib.Path(path).name, pathlib.Path(self.path, path))

    def list_bundles(self) -> Sequence[Bundle]:
        pass

    def list_files(
        self,
        kind: FileType = FileType.any,
        recursive: Optional[bool] = False,
        server: Optional[int] = None,
    ) -> Sequence[PathLike]:
        if self.per_server:
            return kind.list_file_matches(self.path / server, recursive)
        else:
            return kind.list_file_matches(self.path, recursive)

    def random(
        self,
        kind: FileType = FileType.any,
        recursive: Optional[bool] = False,
        server: Optional[int] = None,
    ) -> discord.File:
        """Select a random file from the bundle.

        Args:
            kind: The type of file to choose from.
            recursive: If true, files in subdirectories will be included in the
                choices.
        """
        if self.per_server:
            result: pathlib.Path = random.choice(
                kind.list_file_matches(self.path / server, recursive)
            )
        else:
            result: pathlib.Path = random.choice(
                kind.list_file_matches(self.path, recursive)
            )

        return discord.File(result.read_bytes(), result.name)
