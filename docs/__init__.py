import os


def dev():
    os.system(
        "sphinx-autobuild --watch src/belchan"
        " --ignore docs/source/developer/reference  docs/source docs/build/html"
    )


def build():
    os.system("sphinx-build -b html docs/source docs/build/html")
