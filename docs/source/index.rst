.. belchan documentation master file, created by
   sphinx-quickstart on Fri Feb 26 06:16:18 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to belchan's documentation!
###################################

.. toctree::
   :maxdepth: 2

   user/index
   developer/index

..
   * :doc:`User Documentation <user/index>`
   * :doc:`Developer Documentation <developer/index>`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
