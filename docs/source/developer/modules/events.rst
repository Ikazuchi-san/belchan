Events and Listeners
====================

You can make your module listen to different events by adding a function and
marking it as listener.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   from belchan import commands
   from belchan.models.discord import Event

   class MyModule(Module, ...):
       # Name your function after the event you want to listen for.
       @commands.listener()
       async def on_ready(self):
           self.bot.logger.info("Module is loaded and bot is ready!")

       # Instead of naming your function after the event you want to listen
       # for, you can just provide the event type.
       @commands.listener(Event.shard_ready)
       async def any_name(self, shard_id: int):
           self.bot.logger.info(f"Module is loaded and ready on shard {shard_id}!")
