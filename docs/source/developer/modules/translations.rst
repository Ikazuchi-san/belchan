Translating your Module
=======================

Belchan has a builtin translation system that will let users use your module in
different languages on a per-server basis.

To use it, simply wrap any strings (messages) returned to the user with the
translation functions. If you followed the examples above, you have already used
them.

Mark a string as translatable (lazy translation string)
  :func:`belchan.i18n.N_` or :class:`belchan.i18n.lazy_gettext`

  This is usually used when you no context is available, for example outside of
  commands and for any documentation string.

Translating simple strings
  :func:`belchan.i18n._` or :func:`belchan.i18n.gettext`

  The go-to translation function. Provide the locale to translate the message to
  as the second parameter and it will return the translated string.

Translating pluralizable strings
  :func:`belchan.i18n.ngettext`

All messages now have to be placed in a translation file inside your module's
``translations`` directory. The translation files must be `toml
<https://toml.io>`_ files named after the the `ISO-639-1 code
<https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes>`_ of the language you
are translating to.

Currently, this has to be done by hand in the following format:

.. tab:: Normal

   Normal messages are any messages that do not have a plural form. The
   translated message must be given as ``other``.

   .. code-block:: toml
      :linenos:

      ["Translation String Here"]
      description = "Description Here. This is optional and will not be read."
      other = "Translated Text Here"

.. tab:: Pluralizable

   Pluralizable messages are any messages that have a distinction between the
   message for **1** (``one``) and **many** (``other``).

   .. code-block:: toml
      :linenos:

      ["Singular Translation String Here"]
      description = "Description Here. This is optional and will not be read."
      one = "Translated, Singular Text Here"
      other = "Translated, Pluralized Text Here"

   .. note:: If ``one`` is not specified, ``other`` will automatically be used as a
      fallback.

You can reference the translation files of the standard modules to see it in
action.

.. important:: As base language for translations, you should provide english
   (``en.toml``), as this is the default language for servers. Not providing
   this translation will cause an internal exception on each call, slowing down
   the bot.
