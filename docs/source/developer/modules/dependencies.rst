Managing Python Dependencies
============================

When you want to use external packages (e.g. from `PyPI <https://pypi.org/>`_),
go into your module directory (where the ``pyproject.toml`` is located). Once
you're inside the directory, you can use `Poetry <https://python-poetry.org/>`_
to manage your dependencies.

.. tab:: Updating Dependencies

   .. code-block:: console

      # poetry update

.. tab:: Removing Dependencies

   .. code-block:: console

      # poetry remove <dependency_name>

.. tab:: Adding Dependencies

   .. tab:: By Name from PyPI

     .. code-block:: console

        # poetry add <dependency_name>

   .. tab:: From a local path

     .. code-block:: console

        # poetry add ./path/to/your/dependency

     .. caution:: Dependencies of this kind should be placed within the module
        folder itself and not use absolute paths.

     .. note:: This requires your dependency to have a setup.py and will install
        it in the current virtualenv.

        If your dependency does not have a setup.py, you can manually add it to
        the pyproject.toml under ``[tool.poetry.dependencies]`` like this:

        .. code-block:: toml

           [tool.poetry.dependencies]
           your_dependency_name = { path="path/to/your/dependency", develop = true }

   .. tab:: From a git repository

     .. tab:: Latest

        .. code-block:: console

           # poetry add your_dependency --git https://git.host/project/path

     .. tab:: Spefific branch

        .. code-block:: console

           # poetry add your_dependency --git https://git.host/project/path#branch_name

     .. tab:: Spefific Tag

        .. code-block:: console

           # poetry add your_dependency --git https://git.host/project/path#tag_name
