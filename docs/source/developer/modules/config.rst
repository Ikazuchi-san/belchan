Configuration
=============

Some modules require configuration variables to function. Instead of each module
having to implement their own logic for this, belchan provides an easy to use,
model-based configuration system.

Defining Configurations
-----------------------

Each module can have its own configuration class.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   from typing import Optional

   from belchan.models.base import FIeld
   from belchan.models.config import ModuleConfiguration
   from belchan.models.discord import TextChannel

   class MyModuleConfig(ModuleConfiguration):
       ping_response_message: Optional[str] = Field("pong", description=("The message"
           " to send when receiving a ping."))
       # The value given as "description" should be given. It is used as help string
       # for the option.
       #
       # All fields should either be `Optional` or have a default value other
       # than None.
       ping_response_channel: Optional[TextChannel] = Field(None,
           description=("The channel where the response to the ping command should"
                        " be sent."))

To assign the configuration class to your module, simply set it as ``config`` value during
the module definition.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   class MyModule(Module, doc=ModuleDocumentation(name="My Module", doc=N_("Tutorial bot module.")),
                  config=MyModuleConfig):
       pass

.. warning:: Do not use :mod:`discord` types for configuration. Instead use
   the types from :mod:`belchan.models.discord`.


.. warning:: While being based on `pydantic <https://pydantic-docs.helpmanual.io/>`_,
   configuration currently cannot use other :class:`pydantic.BaseModel`-based
   classes as option types. Instead, use prefixes and :func:`pydantic.Field`'s
   ``alias`` parameter or simply underscores in the variable name.

   .. code-block:: python
      :linenos:
      :caption: Example

      class MyModuleConfig(ModuleConfiguration):
          # Accessible as ping.response_message.
          response_message: Optional[str] = Field(None, alias="ping.response_message", description=...)
          # Accessible as ping_repsonse_channel.
          ping_response_channel: Optional[TextChannel] = Field(None, description=...)

Accessing Configurations inside Commands
----------------------------------------

Module configuration variables can be accessed by using
:attr:`belchan.context.ContextBase.module_config`.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   class MyModuleConfig(ModuleConfiguration):
       ping_response_channel: Optional[TextChannel] = Field(None, description=...)

   class MyModule(Module, ...):
       @command(...)
       async def ping(self, ctx: ContextBase):
           cfg: LogConfig = ctx.module_config

           channel_ = ctx.guild.get_channel(ctx.ping_response_channel)

           if channel_: # For config options without a non-None default value,
                        # always check before accessing their members.
               channel = await ctx.guild.get_channel(channel_.id)
           else:
               channel = ctx.channel

            await channel.send("pong")

Similarly, to access server configuration variables, you can use
:attr:`belchan.context.ContextBase.server_config`.

Disabling Commands
------------------

Modules can be disabled on a per-server basis using the ``enabled`` configuration
option.

Due to how the :func:`belchan.commands.command`-decorator works (it is not
context-aware), this can't be done automatically.
Instead functions need to be decorated with the
:func:`belchan.permissions.require_enabled` decorator.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   from belchan.permissions import require_enabled

   class MyModule(Module, ...):
       @command(doc=CommandDocumentation(command="ping", doc=N_("Returns `pong`")))
       @require_enabled("mymodule") # Lowercase module class-name.
                                     # MyModule.qualified_name.lower() would also work.
       async def ping(self, ctx: ContextBase):
           await ctx.send("pong")
