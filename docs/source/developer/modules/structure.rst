Structure
=========

A module is a folder with the following base structure:

.. code-block::

   your_module
   ├── __init__.py
   ├── pyproject.toml
   └── translations
       └── en.toml

Where ``your_module`` is the name of your module.

The ``__init__.py`` is the entrypoint of your module. You can either put all
your code here, or import from other files as needed.

.. attention::
   Developers used to Python will recognize this as the normal structure of a
   package (if you ignore the ``pyproject.toml`` file). With Belchan, however,
   there are a few quirks:

   * You cannot do relative imports to parent packages (``..``), instead you must
     import starting from the root module. So to import :mod:`belchan.application`,
     you actually have to write:

     .. code-block:: python

        # To import the module itself
        import belchan.application
        # Or to import something from the module
        from belchan.application import Application

   * Globals from other packages do not keep the state between modules; each module
     has a sandboxed Python state. This means that setting a variable, for example
     :data:`belchan.i18n.INSTALLED_LOCALES` will not have any effect outside of the
     module itself.

     If you want to store data in a way that is accessible to both other modules
     and the bot as a whole, use :attr:`belchan.application.Application.data`
     instead. This variable will be accessible inside your module as
     ``self.bot.data``.

.. tab:: __init__.py

   .. code-block:: python
      :linenos:

      # Include the Application class to avoid type checking errors.
      from belchan.application import Application
      # Include the Module class.
      from belchan.modules import Module
      # Module documentations are mandatory. A module without documentation will
      # raise an exception when loaded.
      from belchan.models.docs import ModuleDocumentation
      # To translate strings for the documentation, import :func:`belchan.i18n.N_`
      from belchan.i18n import N_

      # The "doc=" is mandatory, more about this below.
      class MyModule(Module, doc=ModuleDocumentation(name="My Module", doc=N_("Tutorial bot module."))):
         pass

      # This function is called whenever the module is loaded. It constructs the
      # module instance and adds it to the bot.
      def setup(bot: Application):
         bot.add_module(MyModule(bot))

      # This function is called whenever the module is unloaded. It removes the
      # module from the bot (by name).
      def teardown(bot: Application):
         bot.remove_module(Admin.qualified_name)

.. tab:: pyproject.toml

   Simply run the following command inside the new module directory and follow
   the on-screen instructions:

   .. code-block:: console

      # poetry init

   The newly generated ``pyproject.toml`` file file will look similar to this:

   .. code-block:: toml
      :linenos:
      :caption: pyproject.toml

      [tool.poetry]
      name = "belchan_module_general"
      version = "0.1.0"
      description = ""
      authors = ["Your Name <you@example.com>"]

      [tool.poetry.dependencies]
      python = "^3.7"

      [tool.poetry.dev-dependencies]

      [build-system]
      requires = ["poetry-core>=1.0.0"]
      build-backend = "poetry.core.masonry.api"

   If the Python version under ``[tool.poetry.dependencies]`` does not say
   ``^3.7``, please change it to this value unless your external packages
   require a higher version.
