Commands
========

Commands can be added to modules by decorating a function with
:func:`belchan.commands.command`.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   from belchan.commands import command
   # This is required for providing type information for the `ctx` argument.
   from belchan.context import ContextBase
   # This import is required to generate the command documentation. The
   # documentation is mandatory and not providing it will result in an
   # exception and your module will not load.
   from belchan.models.docs import CommandDocumentation
   # To translate strings for the documentation, import :func:`belchan.i18n.N_`
   from belchan.i18n import N_

   # "doc=" is mandatory, more about this below.
   class MyModule(Module, doc=ModuleDocumentation(name="My Module", doc=N_("Tutorial bot module."))):
       # This turns the function following it into a command.
       # As mentioned before, `doc` is mandatory.
       @command(doc=CommandDocumentation(command="ping", doc=N_("Returns `pong`")))
       async def ping(self, ctx: ContextBase):
           await ctx.send("pong")

Documenting Your Command
------------------------

Like modules require a :class:`belchan.models.docs.ModuleDocumentation`,
commands require a :class:`belchan.models.docs.CommandDocumentation`.

A few things to note to get you started:

* For each argument your command takes,
  a :class:`belchan.models.docs.ArgumentDocumentation` should be provided in
  :attr:`belchan.models.docs.CommandDocumentation.args`.
* Aliases can be provided using :attr:`belchan.models.docs.CommandDocumentation.aliases`
* :attr:`belchan.models.docs.CommandDocumentation.doc` and
  :attr:`belchan.models.docs.ArgumentDocumentation.doc` should be a
  :attr:`belchan.i18n.lazy_gettext` created using :attr:`belchan.i18n.N_`.
* Add examples on how to use the command as
  :class:`belchan.models.docs.CommandExample` under
  :attr:`belchan.models.docs.CommandDocumentation.examples`.

A complete command with documentation will look something like this:

.. code-block:: python
   :linenos:

   from belchan.i18n import _, N_
   from belchan.models.docs import (
       CommandDocumentation,
       ArgumentDocumentation,
       CommandExample,
   )

   @command(
       doc=CommendDocumentation(
           command="greet",
           doc=N_("Greets a user."),
           args=[
               ArgumentDocumentation(
                   name="user",
                   doc=N_("The user to greet."),
                   optional=True
               ),
           ],
           examples=[
               CommandExample(
                   command="greet",
                   result=N_("Hello, @you!"),
               ),
               CommandExample(
                   command="greet SomeUser",
                   result=N_("Hello, @SomeUser!"),
               ),
           ]
       )
   )
   async def greet(self, ctx: ContextBase, user: Optional[discord.User] = None):
       if user is None:
           user = ctx.author

       await ctx.send(_("Hello, {user.mention}!", locale=ctx.locale).format(user=user))
