Translations
============

Belchan has the aim to be easily translated into other languages. To
this end, harder to use systems such as `gettext
<https://en.wikipedia.org/wiki/Gettext>`_ are ignored in favor of
simple, hand-writable files that do not need to be processed using
external tools.

.. note:: As side-effect of the translation method used here, Belchan
          can be given a custom personality or persona.

          Simply copy and rename your preferred language and edit the
          new translation files.

Translation Files
-----------------

Belchan uses `TOML <https://github.com/toml-lang/toml>`_-files for its
translations, where all languages are in their own files, called a
``Catalog``, where each translation (called ``Message``).

These catalogs consist of `tables <https://toml.io/en/v1.0.0#table>`_
named after each message.

.. code-block:: toml
   :caption: en.toml

   ["{num} apples"]
   description = "Optional description string"
   one = "{num} apple" # This is used if for pluralizable translations
                       # where the "count" is 0. If the count is not 0, other
                       # will be used. Optional for non-pluralizable messages.
   other = "{num} apples" # This is used for all translations and is
                          # always required.

Each module has its own catalogs that will be loaded when the module
is initialized and **override** any conflicting translation messages.

Using the Translations
----------------------

The translations can be used by calling the corresponding function
from :mod:`belchan.i18n`:

Delayed translations (whenever the locale is not yet known, like in module, command and config docs)
  :func:`belchan.i18n.N_` (alias for :class:`belchan.i18n.lazy_gettext`)

Simple, non-pluralizable translations
  :func:`belchan.i18n._` (alias for :func:`belchan.i18n.gettext`)

Pluralizable translations
  * :func:`belchan.i18n.ngettext`
