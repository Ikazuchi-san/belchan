Getting Started
===============

Requirements
------------

To get started with Belchan, you need at least the following:

* `Python 3.7+ <https://www.python.org/>`_
* `Python Virtualenv <https://pypi.org/project/virtualenv/>`_ (Installed by
  default on most systems)
* `Poetry <https://python-poetry.org/>`_ (Can be installed `using pip
  <https://python-poetry.org/docs/#installing-with-pip>`_)

Additionally it is recommended to install the following dependencies:

* `Git <https://git-scm.com/>`_ (Required for automated module management)

Downloading
-----------

.. tab:: From Releases

   Head over to the `Releases <https://gitlab.com/belchan-bot/belchan/-/releases>`_
   page and download the version you want.

   Once downloaded, extract the archive.

.. tab:: Latest Source (not recommended)

   Download as:

   | `zip <https://gitlab.com/belchan-bot/belchan/-/archive/master/belchan-master.zip>`_
   | `tar <https://gitlab.com/belchan-bot/belchan/-/archive/master/belchan-master.tar>`_

   Once downloaded, extract the archive.

.. tab:: Using Git (recommended for Experienced Users)

   .. note::
      This way of installing requires you to have a working
      `Git <https://git-scm.com/>`_ installation on your system.

      Installing Git is outside of the scope of this documentation.

   Although more complex than installing by simply downloading using the web
   interface, the ease of use later on makes this our recommended way of
   acquiring Belchan for self-hosting.

   .. tab:: Using a Git-UI

      If you have a Git GUI, simply use it to download the the repository and
      checkout the latest tag.

      .. code-block::

         https://gitlab.com/belchan-bot/belchan

   .. tab:: Using the Command Line

      First clone the repository to acquire the sources.

      .. code-block:: console

         # git clone https://gitlab.com/belchan-bot/belchan

      After the download completes, check which tags exist. These are the
      releases.

      .. code-block:: console

         # git tag

         stable_v2021-03

      Select the version you want to use using the ``checkout`` command.

      .. code-block:: console

         # git checkout tags/stable_v2021-03


Installing the Dependencies
---------------------------

Installing the dependencies for belchan requires just 2 easy steps:

1) Extract the downloaded files and enter the directory with the
   ``pyproject.toml`` (first level of directories).
2) Open a terminal, run the following command and wait for the installation
   to finish:

   .. code-block:: console

      # poetry install

Creating an Account for your Bot
--------------------------------

Generating the Configuration File
---------------------------------

In the directory with the ``pyproject.toml``, run the following command to
generate an initial config file for you to edit:

.. code-block:: console

   # python -m belchan init-config

This will generate a ``settings.yml`` file in the current directory.
